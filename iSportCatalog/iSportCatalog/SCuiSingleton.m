//
//  SCuiSingleton.m
//  iSportCatalog
//
//  Created by Paul017 on 19/2/15.
//
//

#import "SCuiSingleton.h"
#import "SCuiConstants.h"

@implementation SCuiSingleton

objection_register_singleton(SCuiSingleton)

-(void) awakeFromObjection{
    self.isSlideMenuEnabled = YES;
    self.categoryProductSelected = [[SCCategoryProduct alloc] initWithCategoryName:kOutstandingCategory subcategoryName:nil andIsSelectableCategory:YES];
}

@end
