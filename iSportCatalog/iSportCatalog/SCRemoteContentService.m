//
//  SCRemoteContentService.m
//  iSportCatalog
//
//  Created by Paul017 on 9/8/15.
//
//

#import "SCRemoteContentService.h"
#import "AFNetworking.h"
#import "SCuiConstants.h"
#import "SCProduct.h"

@implementation SCRemoteContentService
objection_register_singleton(SCRemoteContentService)

-(void) requestServerProductsWithCategoryFind:(NSString *)categoryFind andDelegate:(id<SCRemoteContentDelegate>)delegate{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/articles/%@", kProductionServerUrl, categoryFind] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *responseArray = [responseObject objectForKey:@"Articulo"];
        NSMutableArray *products = [[NSMutableArray alloc] init];
        for (NSDictionary *productDictionary in responseArray) {
            SCProduct *product = [[SCProduct alloc] init];
            product = [product initWithID:(int)[productDictionary objectForKey:@"idArticulo"]
                                class:[productDictionary objectForKey:@"descripcionClase"]
                                category:[productDictionary objectForKey:@"descripcionTipo"]
                                client:[productDictionary objectForKey:@"descripcionCategoria"]
                                sport:[productDictionary objectForKey:@"descripcionDeporte"]
                                model:[productDictionary objectForKey:@"nombreModelo"]
                                brand:[productDictionary objectForKey:@"descripcionMarca"]
                                price:[[productDictionary objectForKey:@"precio"] floatValue]
                                state:[productDictionary objectForKey:@"descripcionEstado"]
                                image1URI:[productDictionary objectForKey:@"urlImagenPrincipal"]
                                image2URI:[productDictionary objectForKey:@"urlImagenSecundaria"]
                                andSize:[productDictionary objectForKey:@""]];
            [products addObject:product];
        }
        [delegate productsReturned:products byCategoryFind:categoryFind];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [delegate productsRequestError:error];
    }];
}

-(void) requestServerProductByBarcode:(NSString*) barcode andDelegate:(id<SCRemoteContentDelegate>)delegate{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/product/%@", kProductionServerUrl, barcode] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (responseObject) {
            SCProduct *product = [[SCProduct alloc] init];
            product = [product initWithID:(int)[responseObject objectForKey:@"idArticulo"]
                                    class:[responseObject objectForKey:@"descripcionClase"]
                                 category:[responseObject objectForKey:@"descripcionTipo"]
                                   client:[responseObject objectForKey:@"descripcionCategoria"]
                                    sport:[responseObject objectForKey:@"descripcionDeporte"]
                                    model:[responseObject objectForKey:@"nombreModelo"]
                                    brand:[responseObject objectForKey:@"descripcionMarca"]
                                    price:[[responseObject objectForKey:@"precio"] floatValue]
                                    state:[responseObject objectForKey:@"descripcionEstado"]
                                image1URI:[responseObject objectForKey:@"urlImagenPrincipal"]
                                image2URI:[responseObject objectForKey:@"urlImagenSecundaria"]
                                  andSize:[responseObject objectForKey:@"talla"]];
            product.productBarcode = barcode;
            [delegate productByBarcodeReturned:product];
        } else {
            [delegate productByBArcodeRequestError:@"Producto no encontrado"];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [delegate productByBArcodeRequestError:error.description];
    }];
}
@end
