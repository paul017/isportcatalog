//
//  main.m
//  iSportCatalog
//
//  Created by Paul017 on 21/03/14.
//
//

#import <UIKit/UIKit.h>

#import "SCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SCAppDelegate class]));
    }
}
