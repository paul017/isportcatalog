//
//  SCObjectionModule.m
//  iSportCatalog
//
//  Created by Paul017 on 19/2/15.
//
//

#import "SCObjectionModule.h"
#import "SCContentProtocol.h"
#import "SCContentFacade.h"
#import "SCRemoteContentProtocol.h"
#import "SCRemoteContentService.h"

@implementation SCObjectionModule

-(void)configure{
    [self bindClass:[SCContentFacade class] toProtocol:@protocol(SCContentProtocol)];
    [self bindClass:[SCRemoteContentService class] toProtocol:@protocol(SCRemoteContentProtocol)];
}

@end
