//
//  SCStoreTableViewCell.h
//  iSportCatalog
//
//  Created by Paul017 on 7/3/15.
//
//

#import <UIKit/UIKit.h>
#import "SCStore.h"

@interface SCStoreTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *storeAddressLabel;
@end
