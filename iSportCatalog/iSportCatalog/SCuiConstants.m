//
//  SCuiConstants.m
//  iSportCatalog
//
//  Created by Paul017 on 19/2/15.
//
//

#import "SCuiConstants.h"

@implementation SCuiConstants

#pragma URL server
NSString * const kProductionServerUrl = @"http://server-isportcatalog.rhcloud.com/iSportCatalogServer/rest";

#pragma UI-Elements NIB
NSString * const kProductDetailTableViewCellNIB = @"SCProductDetailTableViewCell";

#pragma General Font
NSString * const kGeneralFontName = @"HelveticaNeue";

#pragma Notification center - Notification names
NSString * const kReloadCarouselItems = @"ReloadCarouselItems;";
NSString * const kSlideMenuButtonTapped = @"SlideMenuButtonTapped";
NSString * const kSlideMenuViewEnabled = @"SlideMenuViewEnabled";
NSString * const kSlideMenuItemChanged = @"SlideMenuItemChanged";
NSString * const kShowProductDetail = @"ShowProductDetail";
NSString * const kShowErrorServerMessage = @"ShowErrorServerMessage";

#pragma Notification center - Notification info keys
NSString * const kIsSlideMenuViewEnabled = @"IsSlideMenuViewEnabled";

#pragma Storyboard IDs
NSString * const kMapCustomAnnotation = @"MapCustomAnnotation";
NSString * const kMainTabBarController = @"MainTabBarController";
NSString * const kMainCatalogViewController = @"MainCatalogViewController";
NSString * const kSlideMenuViewController = @"SlideMenuViewController";
NSString * const kMenuTableViewCell = @"menuTableViewCell";
NSString * const kProductDetailInfoTableViewCell = @"ProductDetailInfoTableViewCell";
NSString * const kProductDetailCollectionViewCell = @"ProductDetailCollectionViewCell";
NSString * const kStoreTableViewCell = @"StoreTableViewCell";

#pragma Images
NSString * const kPlaceholderImage = @"placeholder.png";
NSString * const kMenuBackgroundImage = @"Window Background";
NSString * const kSlideMenuButtonImage = @"Left Reveal Icon";
NSString * const kOutstandingMenuImage = @"discount";
NSString * const kClothesMenuImage = @"shirt";
NSString * const kFootwearMenuImage = @"shoes";
NSString * const kAccessoryMenuImage = @"cap";
NSString * const kPinMapLocalizationImage = @"pin";

#pragma Segues
NSString * const kMainCatalogToProductSegue = @"MainCatalogToProductSegue";
NSString * const kScanToProductDetailSegue = @"ScanToProductDetailSegue";
NSString * const kLocalizationToMapSegue = @"LocalizationToMapSegue";

#pragma Categories
NSString * const kOutstandingCategory = @"Destacado";
NSString * const kClothesCategory = @"Ropa";
NSString * const kFootwearCategory = @"Calzado";
NSString * const kAccessoryCategory = @"Accesorios";
NSString * const kMenSubcategory = @"Hombres";
NSString * const kWomenSubcategory = @"Mujeres";
NSString * const kChildrenSubcategory = @"Niños";

#pragma Brand Popover
NSString * const kBrandPopoverTitle = @"Seleccione...";
NSString * const kDefautBrandText = @"Marcas";
NSString * const kAllBrandsText = @"Todas";

#pragma Product Info Popover
NSString * const kInfoPopoverTitle = @"+ INFO";
NSString * const kArticleNameText = @"Artículo:";
NSString * const kArticleModelText = @"Modelo:";
NSString * const kArticleSportTex = @"Deporte:";
NSString * const kArticleBrandText = @"Marca:";
NSString * const kArticleSizeText = @"Tamaño:";
NSString * const kArticlePriceText = @"Precio:";

#pragma Default Text
NSString * const kDefaultScanText = @"(none)";
NSString * const kDispositivePositionMapText = @"Mi posición";

@end
