//
//  SCLocalizationViewController.m
//  iSportCatalog
//
//  Created by Paul017 on 19/2/15.
//
//

#import "SCLocalizationViewController.h"
#import "SCuiConstants.h"
#import "SCuiSingleton.h"
#import "SCStore.h"
#import "SCStoreTableViewCell.h"

@interface SCLocalizationViewController ()
@property SCuiSingleton *uiSingleton;
@property (nonatomic) NSArray *storeItems;
@end

@implementation SCLocalizationViewController

objection_requires(@"uiSingleton")
@synthesize uiSingleton = _uiSingleton;
@synthesize storeItems = _storeItems;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[JSObjection defaultInjector] injectDependencies:self];
    // Do any additional setup after loading the view.
    [self setUpStoreItems];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    if (_uiSingleton.isSlideMenuEnabled) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kSlideMenuViewEnabled object:nil userInfo:@{kIsSlideMenuViewEnabled: @NO}];
        _uiSingleton.isSlideMenuEnabled = NO;
    }
    
}

#pragma mark - UITableViewDataSource

-(void) setUpStoreItems{
    //FIXME: lectura archivo
    _storeItems = @[
                    [[SCStore alloc] initWithStoreName:@"Quicentro Shoping" storeCity:@"Quito" storeAddress:@"Av. Naciones Unidas y Av. 6 de Diciembre" storePhoneNumber:@"2222-222" storeLatitudeCoordinate:-0.176210 andStoreLongitudeCoordinate:-78.479770],
                    [[SCStore alloc] initWithStoreName:@"Centro Comercial Iñaquito" storeCity:@"Quito" storeAddress:@"Av. Amazonas y Naciones Unidas" storePhoneNumber:@"2222-222" storeLatitudeCoordinate:-0.1773773 andStoreLongitudeCoordinate:-78.4851448],
                    [[SCStore alloc] initWithStoreName:@"Condado Shoping" storeCity:@"Quito" storeAddress:@"Av. Mariscal Sucre y Avenida John F.Kennedy" storePhoneNumber:@"2222-222" storeLatitudeCoordinate:-0.103858 andStoreLongitudeCoordinate:-78.490314],
                    [[SCStore alloc] initWithStoreName:@"Centro Comercial El Bosque" storeCity:@"Quito" storeAddress:@"Av. Del Parque y Alonso de Torres" storePhoneNumber:@"2222-222" storeLatitudeCoordinate:-0.161868 andStoreLongitudeCoordinate:-78.498058],
                    [[SCStore alloc] initWithStoreName:@"Centro Comercial El Recreo" storeCity:@"Quito" storeAddress:@"Av. Pedro Vicente Maldonado" storePhoneNumber:@"2222-222" storeLatitudeCoordinate:-0.252864 andStoreLongitudeCoordinate:-78.522592],
                    [[SCStore alloc] initWithStoreName:@"Quicentro Sur" storeCity:@"Quito" storeAddress:@"Avenida Morán Valverde y Quitumbe" storePhoneNumber:@"2222-222" storeLatitudeCoordinate:-0.285477 andStoreLongitudeCoordinate:-78.543406],
                    [[SCStore alloc] initWithStoreName:@"Scala Shopping" storeCity:@"Quito" storeAddress:@"Av. Interoceánica Km 12 y Pasaje El Valle" storePhoneNumber:@"2222-222" storeLatitudeCoordinate:-0.206913 andStoreLongitudeCoordinate:-78.426169]
                    
                    ];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_storeItems count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SCStoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kStoreTableViewCell];
    SCStore *store = [_storeItems objectAtIndex:indexPath.row];
    cell.storeNameLabel.text = store.storeName;
    cell.storeAddressLabel.text = store.address;
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _uiSingleton.storeSelected = [_storeItems objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:kLocalizationToMapSegue sender:self];
    
}

/**
 
 -(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UIViewController *destinationVC = segue.destinationViewController;
    
}*/
@end

