//
//  SCMenuViewController.m
//  iSportCatalog
//
//  Created by Paul017 on 10/2/15.
//
//

#import "SCMenuViewController.h"
#import "SCuiConstants.h"
#import "SCMenuTableViewCell.h"
#import "SCCategoryProduct.h"
#import "SCuiSingleton.h"

@interface SCMenuViewController ()

@property (nonatomic) SCuiSingleton *uiSingleton;
@property (nonatomic, strong) UIBarButtonItem *paneRevealLeftBarButtonItem;
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (nonatomic) NSMutableArray *menuItems;

@end

@implementation SCMenuViewController

objection_requires(@"uiSingleton")

@synthesize uiSingleton = _uiSingleton;
@synthesize menuItems = _menuItems;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[JSObjection defaultInjector] injectDependencies:self];
    _menuItems = [[NSMutableArray alloc] initWithArray:[self setUpCategoryItems]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSArray *)setUpCategoryItems{
  
  return @[
           [[SCCategoryProduct alloc] initWithCategoryName:kOutstandingCategory subcategoryName:nil andIsSelectableCategory:YES],
           [[SCCategoryProduct alloc] initWithCategoryName:kClothesCategory subcategoryName:nil andIsSelectableCategory:NO],
            [[SCCategoryProduct alloc] initWithCategoryName:kFootwearCategory subcategoryName:nil andIsSelectableCategory:NO],
            [[SCCategoryProduct alloc] initWithCategoryName:kAccessoryCategory subcategoryName:nil andIsSelectableCategory:YES]
           ];
}

//Register listener notifications
-(void)setUpNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dynamicsDrawerRevealLeftBarButtonItemTapped:) name:kSlideMenuButtonTapped object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paneDragRevealEnabled:) name:kSlideMenuViewEnabled object:nil];
}

// Transition view controller when the category is selected in menu
- (void)transitionToViewController:(NSString *)paneViewControllerType
{
    // Close pane if already displaying the pane view controller
    if ([paneViewControllerType isEqualToString:self.paneViewControllerType]) {
        [self.dynamicsDrawerViewController setPaneState:MSDynamicsDrawerPaneStateClosed animated:YES allowUserInterruption:YES completion:nil];
        return;
    }
    
    BOOL animateTransition = self.dynamicsDrawerViewController.paneViewController != nil;
    
    UITabBarController *paneTabBarController = [self.storyboard instantiateViewControllerWithIdentifier:kMainTabBarController];
    
    [self.dynamicsDrawerViewController setPaneViewController:paneTabBarController animated:animateTransition completion:nil];

    self.paneViewControllerType = paneViewControllerType;
    
    [self setUpNotifications];
}

- (void)dynamicsDrawerRevealLeftBarButtonItemTapped:(id)sender{
    [self.dynamicsDrawerViewController setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:YES completion:nil];
}

- (void)paneDragRevealEnabled:(NSNotification *)notification{
    BOOL enabled = [[notification.userInfo valueForKey:kIsSlideMenuViewEnabled] boolValue];
   [self.dynamicsDrawerViewController setPaneDragRevealEnabled:enabled forDirection:MSDynamicsDrawerDirectionLeft];
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_menuItems count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SCCategoryProduct *categoryProduct = [_menuItems objectAtIndex:indexPath.row];
    SCMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMenuTableViewCell];
    if (categoryProduct.subcategoryProductName!=nil) {
        cell.categoryNameLabel.text = categoryProduct.subcategoryProductName;
        cell.categoryNameLabel.font = [cell.categoryNameLabel.font fontWithSize:13];
        //cell.categoryNameLabel.textColor = [UIColor colorWithRed:0.0 green:(144.0/255.0) blue:(215.0/255.0) alpha:1.0];
        
    } else {
        cell.categoryNameLabel.text = categoryProduct.categoryProductName;
        if ([cell.categoryNameLabel.text isEqualToString:kOutstandingCategory])
            [cell.categoryImage setImage:[UIImage imageNamed:kOutstandingMenuImage]];
        else if ([cell.categoryNameLabel.text isEqualToString:kClothesCategory])
            [cell.categoryImage setImage:[UIImage imageNamed:kClothesMenuImage]];
        else if ([cell.categoryNameLabel.text isEqualToString:kFootwearCategory])
            [cell.categoryImage setImage:[UIImage imageNamed:kFootwearMenuImage]];
        else
            [cell.categoryImage setImage:[UIImage imageNamed:kAccessoryMenuImage]];
            
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SCCategoryProduct *categoryProduct = [_menuItems objectAtIndex:indexPath.row];
    
    if (!categoryProduct.isSelectableProductCategory) {
        if ([_menuItems count] == 4) {
            [self addNextCellsForIndex:(int)indexPath.row withCategory:categoryProduct.categoryProductName ofTableView:tableView];
            _uiSingleton.categoryProductSelected = categoryProduct;
        } else {
            if ([categoryProduct.categoryProductName isEqualToString:_uiSingleton.categoryProductSelected.categoryProductName]) {
                [self removeNextCellsForIndex:(int)indexPath.row ofTableView:tableView];
            }else if ([_uiSingleton.categoryProductSelected.categoryProductName isEqualToString:kClothesCategory]){
                [self removeNextCellsForIndex:1 ofTableView:tableView];
                [self addNextCellsForIndex:2 withCategory:categoryProduct.categoryProductName ofTableView:tableView];
            }else if ([_uiSingleton.categoryProductSelected.categoryProductName isEqualToString:kFootwearCategory]){
                [self removeNextCellsForIndex:2 ofTableView:tableView];
                [self addNextCellsForIndex:1 withCategory:categoryProduct.categoryProductName ofTableView:tableView];
            }
            _uiSingleton.categoryProductSelected = categoryProduct;
        }
    } else {
        if (categoryProduct.subcategoryProductName == nil && [_menuItems count] > 4) {
            if ([_uiSingleton.categoryProductSelected.categoryProductName isEqualToString:kClothesCategory]) {
                [self removeNextCellsForIndex:1 ofTableView:tableView];
            } else if ([_uiSingleton.categoryProductSelected.categoryProductName isEqualToString:kFootwearCategory]){
                [self removeNextCellsForIndex:2 ofTableView:tableView];
            }
        }
        _uiSingleton.categoryProductSelected = categoryProduct;
        [[NSNotificationCenter defaultCenter] postNotificationName:kSlideMenuItemChanged object:nil];
        [self.dynamicsDrawerViewController setPaneState:MSDynamicsDrawerPaneStateClosed animated:YES allowUserInterruption:YES completion:nil];
    }
    
}

-(void) removeNextCellsForIndex:(int)index ofTableView:(UITableView *)tableView{
    [_menuItems removeObjectAtIndex:index+1];
    [_menuItems removeObjectAtIndex:index+1];
    [_menuItems removeObjectAtIndex:index+1];
    
    [tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index+1 inSection:0],
                                        [NSIndexPath indexPathForRow:index+2 inSection:0],
                                        [NSIndexPath indexPathForRow:index+3 inSection:0],] withRowAnimation:UITableViewRowAnimationAutomatic];
}


-(void) addNextCellsForIndex:(int)index withCategory: (NSString *)category ofTableView:(UITableView *)tableView{
    [_menuItems insertObject: [[SCCategoryProduct alloc] initWithCategoryName:category subcategoryName:kMenSubcategory andIsSelectableCategory:YES] atIndex: index+1];
    [_menuItems insertObject: [[SCCategoryProduct alloc] initWithCategoryName:category subcategoryName:kWomenSubcategory andIsSelectableCategory:YES] atIndex: index+2];
    [_menuItems insertObject: [[SCCategoryProduct alloc] initWithCategoryName:category subcategoryName:kChildrenSubcategory andIsSelectableCategory:YES] atIndex: index+3];
    
    
    [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index+1 inSection:0],
                                        [NSIndexPath indexPathForRow:index+2 inSection:0],
                                        [NSIndexPath indexPathForRow:index+3 inSection:0],] withRowAnimation:UITableViewRowAnimationAutomatic];
}
@end
