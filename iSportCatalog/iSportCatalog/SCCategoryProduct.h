//
//  SCCategoryProduct.h
//  iSportCatalog
//
//  Created by Paul017 on 7/3/15.
//
//

#import <Foundation/Foundation.h>

@interface SCCategoryProduct : NSObject

@property (nonatomic) NSString *categoryProductName;
@property (nonatomic) NSString *subcategoryProductName;
@property (nonatomic) BOOL isSelectableProductCategory;

-(SCCategoryProduct *) initWithCategoryName:(NSString *)categoryName subcategoryName:(NSString *)subcategoryName andIsSelectableCategory:(BOOL) isSelectableCategory;

@end
