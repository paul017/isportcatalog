//
//  SCuiConstants.h
//  iSportCatalog
//
//  Created by Paul017 on 19/2/15.
//
//

#import <Foundation/Foundation.h>

@interface SCuiConstants : NSObject

#define SC_LIGHT_BLUE_COLOR [UIColor colorWithRed:0.0 green:(144.0/255.0) blue:(215.0/255.0) alpha:1.0]
#define SC_BLUE_COLOR [UIColor colorWithRed:0.0 green:(114.0/255.0) blue:(215.0/255.0) alpha:1.0]
#define SC_DARK_BLUE_COLOR [UIColor colorWithRed:(9.0/255.0) green:(54.0/255.0) blue:(81.0/255.0) alpha:1.0]
#define SC_OPAQUE_BLUE_COLOR [UIColor colorWithRed:0.0 green:(85.0/255.0) blue:(125.0/255.0) alpha:0.7]
#define SC_FIX_COLOR [UIColor colorWithHue:0.53 saturation:0.80 brightness:0.70 alpha:1.0]

#pragma URL server
extern NSString * const kProductionServerUrl;

#pragma UI-Elements NIB
extern NSString * const kProductDetailTableViewCellNIB;

#pragma General Font
extern NSString * const kGeneralFontName;

#pragma Notification center - Notification names
extern NSString * const kReloadCarouselItems;
extern NSString * const kSlideMenuButtonTapped;
extern NSString * const kSlideMenuViewEnabled;
extern NSString * const kSlideMenuItemChanged;
extern NSString * const kShowProductDetail;
extern NSString * const kShowErrorServerMessage;

#pragma Notification center - Notification info keys
extern NSString * const kIsSlideMenuViewEnabled;

#pragma Storyboard IDs
extern NSString * const kMapCustomAnnotation;
extern NSString * const kMainTabBarController;
extern NSString * const kMainCatalogViewController;
extern NSString * const kSlideMenuViewController;
extern NSString * const kMenuTableViewCell;
extern NSString * const kProductDetailInfoTableViewCell;
extern NSString * const kProductDetailCollectionViewCell;
extern NSString * const kStoreTableViewCell;

#pragma Images
extern NSString * const kPlaceholderImage;
extern NSString * const kMenuBackgroundImage;
extern NSString * const kSlideMenuButtonImage;
extern NSString * const kOutstandingMenuImage;
extern NSString * const kClothesMenuImage;
extern NSString * const kFootwearMenuImage;
extern NSString * const kAccessoryMenuImage;
extern NSString * const kPinMapLocalizationImage;

#pragma Segues
extern NSString * const kMainCatalogToProductSegue;
extern NSString * const kScanToProductDetailSegue;
extern NSString * const kLocalizationToMapSegue;

#pragma Categories
extern NSString * const kOutstandingCategory;
extern NSString * const kClothesCategory;
extern NSString * const kFootwearCategory;
extern NSString * const kAccessoryCategory;
extern NSString * const kMenSubcategory;
extern NSString * const kWomenSubcategory;
extern NSString * const kChildrenSubcategory;

#pragma Brand Popover
extern NSString * const kBrandPopoverTitle;
extern NSString * const kDefautBrandText;
extern NSString * const kAllBrandsText;

#pragma Product Info Popover
extern NSString * const kInfoPopoverTitle;
extern NSString * const kArticleNameText;
extern NSString * const kArticleModelText;
extern NSString * const kArticleSportTex;
extern NSString * const kArticleBrandText;
extern NSString * const kArticleSizeText;
extern NSString * const kArticlePriceText;

#pragma Default Text
extern NSString * const kDefaultScanText;
extern NSString * const kDispositivePositionMapText;

@end
