//
//  SCProductDetailCollectionViewCell.h
//  iSportCatalog
//
//  Created by Paul017 on 24/3/15.
//
//

#import <UIKit/UIKit.h>
#import "FXImageView.h"

@interface SCProductDetailCollectionViewCell : UICollectionViewCell
@property (nonatomic) IBOutlet FXImageView *productImageView;

@end
