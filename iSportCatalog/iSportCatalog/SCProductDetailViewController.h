//
//  SCProductDetailViewController.h
//  iSportCatalog
//
//  Created by Paul017 on 20/2/15.
//
//

#import <UIKit/UIKit.h>
#import "PopoverView.h"

@interface SCProductDetailViewController : UIViewController <UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate, PopoverViewDelegate>

@end
