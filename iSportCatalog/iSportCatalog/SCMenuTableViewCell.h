//
//  SCMenuTableViewCell.h
//  iSportCatalog
//
//  Created by Paul017 on 25/2/15.
//
//

#import <UIKit/UIKit.h>

@interface SCMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImage;

@end
