//
//  SCAppDelegate.h
//  iSportCatalog
//
//  Created by Paul017 on 21/03/14.
//
//

@class MSDynamicsDrawerViewController;
#import <UIKit/UIKit.h>

@interface SCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MSDynamicsDrawerViewController *dynamicsDrawerViewController;

@end
