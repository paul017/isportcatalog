//
//  SCMapAnnotation.m
//  iSportCatalog
//
//  Created by Paul017 on 5/3/15.
//
//

#import "SCMapAnnotation.h"
#import "SCuiConstants.h"

@implementation SCMapAnnotation

-(id)initWithTitle:(NSString *)newTitle location:(CLLocationCoordinate2D)location{
    self = [super init];
    if (self) {
        _title = newTitle;
        _coordinate = location;
    }
    return self;
}

-(MKAnnotationView *)annotationView{
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:kMapCustomAnnotation];
    
    annotationView.enabled = YES;
    annotationView.canShowCallout = YES;
    annotationView.image = [UIImage imageNamed:kPinMapLocalizationImage];
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    return annotationView;
}

@end
