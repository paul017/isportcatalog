//
//  SCLocalizationDetailViewController.m
//  iSportCatalog
//
//  Created by Paul017 on 20/2/15.
//
//

#import "SCLocalizationDetailViewController.h"
#import "SCuiSingleton.h"
#import "SCuiConstants.h"
#import "SCStore.h"
#import "SCMapAnnotation.h"
@import CoreLocation;

@interface SCLocalizationDetailViewController ()
@property (nonatomic) SCuiSingleton *uiSingleton;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) SCStore *selectedStore;
@end

@implementation SCLocalizationDetailViewController

objection_requires(@"uiSingleton")
@synthesize uiSingleton = _uiSingleton;
@synthesize selectedStore = _selectedStore;
@synthesize mapView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[JSObjection defaultInjector] injectDependencies:self];
    self.mapView.delegate = self;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;

    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [self.locationManager requestWhenInUseAuthorization];
    }

    //[self.locationManager startUpdatingLocation];
    self.mapView.showsUserLocation = YES;
    MKUserTrackingBarButtonItem *buttonItem = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];
    self.navigationItem.rightBarButtonItem = buttonItem;

    SCMapAnnotation *testAnnotation = [[SCMapAnnotation alloc] initWithTitle:_uiSingleton.storeSelected.storeName location:_uiSingleton.storeSelected.storeCoordinate];
    
    [self.mapView addAnnotation:testAnnotation];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(_uiSingleton.storeSelected.storeCoordinate, 1000, 1000);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    
    if (_uiSingleton.isSlideMenuEnabled) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kSlideMenuViewEnabled object:nil userInfo:@{kIsSlideMenuViewEnabled: @NO}];
        _uiSingleton.isSlideMenuEnabled = NO;
    }
    
}

// Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", [locations lastObject]);
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    userLocation.title = kDispositivePositionMapText;
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    if ([annotation isKindOfClass:[SCMapAnnotation class]]) {
        SCMapAnnotation *testAnnotation = (SCMapAnnotation *)annotation;
        MKAnnotationView *annotationView = [self.mapView dequeueReusableAnnotationViewWithIdentifier:kMapCustomAnnotation];
        
        if (annotationView == nil) {
            annotationView = testAnnotation.annotationView;
        } else
            annotationView.annotation = annotation;
        
        return annotationView;
    } else
        return nil;
}
@end
