//
//  SCStore.h
//  iSportCatalog
//
//  Created by Paul017 on 7/3/15.
//
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface SCStore : NSObject

@property (nonatomic) NSString *city;
@property (nonatomic) NSString *address;
@property (nonatomic) NSString *phoneNumber;
@property (nonatomic) NSString *storeName;
@property (nonatomic) CLLocationCoordinate2D storeCoordinate;

-(SCStore *)initWithStoreName:(NSString *)storeName storeCity:(NSString *)storeCity storeAddress:(NSString *)storeAddress storePhoneNumber:(NSString *)storePhoneNumber storeLatitudeCoordinate:(CLLocationDegrees)storeLatitudeCoordinate andStoreLongitudeCoordinate:(CLLocationDegrees)storeLongitudeCoordinate;

@end
