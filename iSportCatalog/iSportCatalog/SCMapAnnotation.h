//
//  SCMapAnnotation.h
//  iSportCatalog
//
//  Created by Paul017 on 5/3/15.
//
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface SCMapAnnotation : NSObject <MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (copy, nonatomic) NSString *title;

-(id) initWithTitle: (NSString *)newTitle location:(CLLocationCoordinate2D)location;
-(MKAnnotationView *)annotationView;

@end
