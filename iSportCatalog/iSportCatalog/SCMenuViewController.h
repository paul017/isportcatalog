//
//  SCMenuViewController.h
//  iSportCatalog
//
//  Created by Paul017 on 10/2/15.
//
//

#import <UIKit/UIKit.h>
#import "MSDynamicsDrawerViewController.h"

@interface SCMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) MSDynamicsDrawerViewController *dynamicsDrawerViewController;

@property (nonatomic, assign) NSString *paneViewControllerType;
- (void)transitionToViewController:(NSString *)paneViewControllerType;

@end
