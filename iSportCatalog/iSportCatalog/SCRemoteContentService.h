//
//  SCRemoteContentService.h
//  iSportCatalog
//
//  Created by Paul017 on 9/8/15.
//
//

#import <Foundation/Foundation.h>
#import "SCRemoteContentProtocol.h"

@interface SCRemoteContentService : NSObject <SCRemoteContentProtocol>

@end
