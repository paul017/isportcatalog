//
//  SCProduct.m
//  iSportCatalog
//
//  Created by Paul017 on 23/3/15.
//
//

#import "SCProduct.h"

@implementation SCProduct

@synthesize productID = _productID;
@synthesize productClass = _productClass;
@synthesize productModel = _productModel;
@synthesize productBrand = _productBrand;
@synthesize productCategory = _productCategory;
@synthesize productClient = _productClient;
@synthesize productSport = _productSport;
@synthesize productPrice = _productPrice;
@synthesize productPrimaryImageURI = _productPrimaryImageURI;
@synthesize productSecondaryImageURI = _productSecondaryImageURI;
@synthesize productState = _productState;
@synthesize productSize = _productSize;



-(SCProduct *)initWithID:(int) productID class:(NSString*)class category:(NSString *)category client:(NSString *)client sport:(NSString *)sport model:(NSString *)model brand:(NSString *)brand price:(float)price state:(NSString*)state image1URI:(NSString *)image1URI image2URI:(NSString *)image2URI andSize:(NSString*)size{
    SCProduct *product = [[SCProduct alloc] init];
    product.productID = productID;
    product.productClass = class;
    product.productCategory = category;
    product.productClient = client;
    product.productSport = sport;
    product.productModel = model;
    product.productBrand = brand;
    product.productPrice = price;
    product.productPrimaryImageURI = image1URI;
    product.productSecondaryImageURI = image2URI;
    product.productState = state;
    product.productSize = size;
    return product;
}

//FIXME: Generacion de productos Quemados
-(SCProduct *)initWithCategory:(NSString *)category client:(NSString *)client sport:(NSString *)sport model:(NSString *)model brand:(NSString *)brand price:(float)price image1URI:(NSString *)image1URI image2URI:(NSString *)image2URI{
    SCProduct *product = [[SCProduct alloc] init];
    product.productCategory = category;
    product.productClient = client;
    product.productSport = sport;
    product.productModel = model;
    product.productBrand = brand;
    product.productPrice = price;
    product.productPrimaryImageURI = image1URI;
    product.productSecondaryImageURI = image2URI;
    return product;
}

-(NSMutableArray *)getOutstandingProducts{
    NSMutableArray *products = [[NSMutableArray alloc] init];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Running" model:@"Flyknit Lunar 3 " brand:@"Nike" price:206.8 image1URI:@"http://goo.gl/hUcNZA" image2URI:@"http://goo.gl/JQsUHJ"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Running" model:@"Zoom Terra Kiger" brand:@"Nike" price:148.47 image1URI:@"http://goo.gl/2R3jRd" image2URI:@"http://goo.gl/kixVsS"]];
    [products addObject:[self initWithCategory:@"Zapatos" client:@"Hombre" sport:@"Fútbol" model:@"HYPERVENOM Phantom FG" brand:@"Nike" price:233.31 image1URI:@"http://goo.gl/q8fpkm" image2URI:@"http://goo.gl/RZiSV3"]];
    [products addObject:[self initWithCategory:@"Zapatos" client:@"Hombre" sport:@"Fútbol" model:@"Elastico Finale III TF" brand:@"Nike" price:84.84 image1URI:@"http://goo.gl/20jWMt" image2URI:@"http://goo.gl/5fB3MX"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Baloncesto" model:@"Air Foamposite One" brand:@"Nike" price:233.31 image1URI:@"http://goo.gl/6jRy4C" image2URI:@"http://goo.gl/0LcB9A"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Baloncesto" model:@"Chuck Posite" brand:@"Nike" price:212.1 image1URI:@"http://goo.gl/HSCPQL" image2URI:@"http://goo.gl/Tol1Jb"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Sportwear" model:@"Air Wildwood Premium RT" brand:@"Nike" price:132.46 image1URI:@"http://goo.gl/ayJvi8" image2URI:@"http://goo.gl/Z6tHqy"]];
    [products addObject:[self initWithCategory:@"Botas" client:@"Hombre" sport:@"Sportwear" model:@"SFB Field 15cm" brand:@"Nike" price:222.54 image1URI:@"http://goo.gl/zZ9ORk" image2URI:@"http://goo.gl/V4fbzt"]];
    [products addObject:[self initWithCategory:@"Sudadera con capucha" client:@"Hombre" sport:@"Baloncesto" model:@"KD7 Hero Premium Full-Zip" brand:@"Nike" price:68.8 image1URI:@"http://goo.gl/Yi4VPM" image2URI:@"http://goo.gl/IU7Jes"]];
    [products addObject:[self initWithCategory:@"Sudadera sin capucha" client:@"Hombre" sport:@"Sportwear" model:@"F.C. AW77 Long-Sleeve Crew" brand:@"Nike" price:71 image1URI:@"http://goo.gl/kxXO1j" image2URI:@"http://goo.gl/Q5l7eV"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Mujer" sport:@"Sportwear" model:@"Air Max Thea" brand:@"Nike" price:129.23 image1URI:@"http://goo.gl/8JoRjg" image2URI:@"http://goo.gl/VaF0Ae"]];
    [products addObject:[self initWithCategory:@"Sandalias" client:@"Mujer" sport:@"Sportwear" model:@"Lunarsandiator Sky Hi" brand:@"Nike" price:115.23 image1URI:@"http://goo.gl/kKDzMD" image2URI:@"http://goo.gl/1tamt2"]];
    [products addObject:[self initWithCategory:@"Sudadera con capucha" client:@"Mujer" sport:@"Running" model:@"Dri-FIT Sprint Half-Zip" brand:@"Nike" price:109.23 image1URI:@"http://goo.gl/lykocO" image2URI:@"http://goo.gl/4eOIn8"]];
    [products addObject:[self initWithCategory:@"Sudadera sin capucha" client:@"Mujer" sport:@"Golf" model:@"Thermal Half-Zip" brand:@"Nike" price:67.72 image1URI:@"http://goo.gl/NzZktV" image2URI:@"http://goo.gl/YzuUUg"]];
    [products addObject:[self initWithCategory:@"Licra" client:@"Mujer" sport:@"Running" model:@"Dri-FIT Epic Run" brand:@"Nike" price:81.92 image1URI:@"http://goo.gl/msrvHB" image2URI:@"http://goo.gl/yKGfeP"]];
    [products addObject:[self initWithCategory:@"Vestido" client:@"Mujer" sport:@"Tennis" model:@"Premier Maria" brand:@"Nike" price:163.85 image1URI:@"http://goo.gl/RdKiAj" image2URI:@"http://goo.gl/gvHRiq"]];
    [products addObject:[self initWithCategory:@"Zapatos" client:@"Niños" sport:@"Fútbol" model:@"Jr. Hypervenom Phelon Premium IC 'Liquid Diamond'" brand:@"Nike" price:65.51 image1URI:@"http://goo.gl/mDczru" image2URI:@"http://goo.gl/UpUazs"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Niños" sport:@"Baloncesto" model:@"Kobe X" brand:@"Nike" price:120.1 image1URI:@"http://goo.gl/FG4kEC" image2URI:@"http://goo.gl/2Luj5b"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Niños" sport:@"Sportwear" model:@"Roshe Run" brand:@"Nike" price:70.97 image1URI:@"http://goo.gl/NJpI77" image2URI:@"http://goo.gl/1i4MOZ"]];
    [products addObject:[self initWithCategory:@"Camiseta" client:@"Niños" sport:@"Sportwear" model:@"HBR" brand:@"Nike" price:18.99 image1URI:@"http://goo.gl/oXweiz" image2URI:@"http://goo.gl/RwiwYZ"]];
    [products addObject:[self initWithCategory:@"Sudadera con capucha" client:@"Niños" sport:@"Running" model:@"Tech Fleece Full-Zip" brand:@"Nike" price:60.08 image1URI:@"http://goo.gl/survkj" image2URI:@"http://goo.gl/ovbv81"]];
    [products addObject:[self initWithCategory:@"Sudadera sin capucha" client:@"Niños" sport:@"Sportwear" model:@"YA76 HBR Crew" brand:@"Nike" price:43.69 image1URI:@"http://goo.gl/aIs7kY" image2URI:@"http://goo.gl/cjiYzW"]];
    [products addObject:[self initWithCategory:@"Pantalón Corto" client:@"Niños" sport:@"Baloncesto" model:@"Kobe Emerge Statement" brand:@"Nike" price:26.75 image1URI:@"http://goo.gl/Uzgz9C" image2URI:@"http://goo.gl/lPvxzM"]];
    [products addObject:[self initWithCategory:@"Bolso" client:@"" sport:@"Sportwear" model:@"Victory Gym Club" brand:@"Nike" price:90 image1URI:@"http://goo.gl/jvztw4" image2URI:@"http://goo.gl/xyfzgN"]];
    [products addObject:[self initWithCategory:@"Calcetines" client:@"" sport:@"Running" model:@"Nike Anti-Blister Lightweight Low-Cut" brand:@"Nike" price:8 image1URI:@"http://goo.gl/i9aA2T" image2URI:@"http://goo.gl/s7x085"]];
    [products addObject:[self initWithCategory:@"Bufandas" client:@"" sport:@"Fútbol" model:@"FC Barcelona Double Stripe Bufanda" brand:@"Nike" price:20.81 image1URI:@"http://goo.gl/YJ7ZQv" image2URI:@""]];
    [products addObject:[self initWithCategory:@"Cinta de Cabello" client:@"" sport:@"Tennis" model:@"DRI-FIT" brand:@"Nike" price:9 image1URI:@"http://goo.gl/z7kdGp" image2URI:@""]];
    return products;
}

-(NSMutableArray *)getClothesMenProducts{
    NSMutableArray *products = [[NSMutableArray alloc] init];
    [products addObject:[self initWithCategory:@"Camiseta" client:@"Hombre" sport:@"Running" model:@"Miler UV" brand:@"Nike" price:32.75 image1URI:@"http://goo.gl/Z2NJdC" image2URI:@"http://goo.gl/dmWwA4"]];
    [products addObject:[self initWithCategory:@"Camiseta" client:@"Hombre" sport:@"Fútbol" model:@"England Core" brand:@"Nike" price:24.5 image1URI:@"http://goo.gl/lsSZoG" image2URI:@"http://goo.gl/PzH0m0"]];
    [products addObject:[self initWithCategory:@"Camiseta" client:@"Hombre" sport:@"Baloncesto" model:@"Jordan AJ All-Season Compression Short-Sleeve" brand:@"Nike" price:38.21 image1URI:@"http://goo.gl/4Wswkk" image2URI:@"http://goo.gl/4uvJOF"]];
    [products addObject:[self initWithCategory:@"Sudadera con capucha" client:@"Hombre" sport:@"Fútbol" model:@"GPX Knit Full-Zip" brand:@"Nike" price:70.97 image1URI:@"http://goo.gl/sgUkXo" image2URI:@"http://goo.gl/lLGEhZ"]];
    [products addObject:[self initWithCategory:@"Sudadera con capucha" client:@"Hombre" sport:@"Baloncesto" model:@"KD7 Hero Premium Full-Zip" brand:@"Nike" price:68.8 image1URI:@"http://goo.gl/Yi4VPM" image2URI:@"http://goo.gl/IU7Jes"]];
    [products addObject:[self initWithCategory:@"Sudadera sin capucha" client:@"Hombre" sport:@"Running" model:@"Element Half-Zip" brand:@"Nike" price:71 image1URI:@"http://goo.gl/k5kzgN" image2URI:@"http://goo.gl/OgSJji"]];
    [products addObject:[self initWithCategory:@"Sudadera sin capucha" client:@"Hombre" sport:@"Sportwear" model:@"F.C. AW77 Long-Sleeve Crew" brand:@"Nike" price:71 image1URI:@"http://goo.gl/kxXO1j" image2URI:@"http://goo.gl/Q5l7eV"]];
    [products addObject:[self initWithCategory:@"Pantalón" client:@"Hombre" sport:@"Baloncesto" model:@" Elite Stripe Performance Fleece" brand:@"Nike" price:53.51 image1URI:@"http://goo.gl/cdSUp5" image2URI:@"http://goo.gl/anIZf0"]];
    [products addObject:[self initWithCategory:@"Pantalón" client:@"Hombre" sport:@"Fútbol" model:@"FC Barcelona Venom Covert" brand:@"Nike" price:76.46 image1URI:@"http://goo.gl/C1Te6p" image2URI:@"http://goo.gl/C3JqaE"]];
    [products addObject:[self initWithCategory:@"Pantalón" client:@"Hombre" sport:@"Sportwear" model:@"Intentional" brand:@"Nike" price:49.15 image1URI:@"http://goo.gl/OYtGJ8" image2URI:@"http://goo.gl/ebs2Zd"]];
    [products addObject:[self initWithCategory:@"Pantalón Corto" client:@"Hombre" sport:@"Fútbol" model:@"2014 Turkey Stadium" brand:@"Nike" price:41.51 image1URI:@"http://goo.gl/WyOsa6" image2URI:@"http://goo.gl/KZPdvZ"]];
    [products addObject:[self initWithCategory:@"Pantalón Corto" client:@"Hombre" sport:@"Running" model:@"5cm Racing" brand:@"Nike" price:49.15 image1URI:@"http://goo.gl/Qk6eh6" image2URI:@"http://goo.gl/bORPaS"]];
    [products addObject:[self initWithCategory:@"Pantalón Corto" client:@"Hombre" sport:@"Baloncesto" model:@"KD Quickness Allover-Print" brand:@"Nike" price:34.4 image1URI:@"http://goo.gl/IeYpb8" image2URI:@"http://goo.gl/L0pka9"]];
    return products;
}

-(NSMutableArray *)getClothesWomenProducts{
    NSMutableArray *products = [[NSMutableArray alloc] init];
    [products addObject:[self initWithCategory:@"Camiseta" client:@"Mujer" sport:@"Running" model:@"Dri-FIT Knit Short-Sleeve" brand:@"Nike" price:61.17 image1URI:@"http://goo.gl/iCqPCh" image2URI:@"http://goo.gl/RLxCTh"]];
    [products addObject:[self initWithCategory:@"Camiseta" client:@"Mujer" sport:@"Tennis" model:@"Slam Tunic" brand:@"Nike" price:65.54 image1URI:@"http://goo.gl/tgVqHz" image2URI:@"http://goo.gl/dS5Rga"]];
    [products addObject:[self initWithCategory:@"Camiseta" client:@"Mujer" sport:@"Sportwear" model:@"Track and Field Seasonal" brand:@"Nike" price:22.93 image1URI:@"http://goo.gl/PrLB7F" image2URI:@"http://goo.gl/zzBX71"]];
    [products addObject:[self initWithCategory:@"Sudadera con capucha" client:@"Mujer" sport:@"Running" model:@"Dri-FIT Sprint Half-Zip" brand:@"Nike" price:109.23 image1URI:@"http://goo.gl/lykocO" image2URI:@"http://goo.gl/4eOIn8"]];
    [products addObject:[self initWithCategory:@"Sudadera con capucha" client:@"Mujer" sport:@"Sportwear" model:@"Gym Vintage Dip Dye Full-Zip" brand:@"Nike" price:54.62 image1URI:@"http://goo.gl/Tk0IvC" image2URI:@"http://goo.gl/VW0G81"]];
    [products addObject:[self initWithCategory:@"Sudadera sin capucha" client:@"Mujer" sport:@"Sportwear" model:@"Gym Vintage Crew Dip Dye" brand:@"Nike" price:36.57 image1URI:@"http://goo.gl/Z6ezhJ" image2URI:@"http://goo.gl/sXIKGK"]];
    [products addObject:[self initWithCategory:@"Sudadera sin capucha" client:@"Mujer" sport:@"Golf" model:@"Thermal Half-Zip" brand:@"Nike" price:67.72 image1URI:@"http://goo.gl/NzZktV" image2URI:@"http://goo.gl/YzuUUg"]];
    [products addObject:[self initWithCategory:@"Pantalón" client:@"Mujer" sport:@"Sportwear" model:@"Bonded Woven Mish Mash" brand:@"Nike" price:71 image1URI:@"http://goo.gl/msMrfB" image2URI:@"http://goo.gl/epzwAe"]];
    [products addObject:[self initWithCategory:@"Pantalón" client:@"Mujer" sport:@"Running" model:@"Storm-FIT 1" brand:@"Nike" price:98.31 image1URI:@"http://goo.gl/gCnHI0" image2URI:@"http://goo.gl/K3fjqG"]];
    [products addObject:[self initWithCategory:@"Licra" client:@"Mujer" sport:@"Running" model:@"Dri-FIT Epic Run" brand:@"Nike" price:81.92 image1URI:@"http://goo.gl/msrvHB" image2URI:@"http://goo.gl/yKGfeP"]];
    [products addObject:[self initWithCategory:@"Pantalón Corto" client:@"Mujer" sport:@"Sportwear" model:@"Gym Vintage Dip Dye" brand:@"Nike" price:35 image1URI:@"http://goo.gl/MHO0Cs" image2URI:@"http://goo.gl/Ta0otw"]];
    [products addObject:[self initWithCategory:@"Pantalón Corto" client:@"Mujer" sport:@"Fútbol" model:@"Gameday Woven" brand:@"Nike" price:25 image1URI:@"http://goo.gl/hzlMK0" image2URI:@"http://goo.gl/f3r8Uy"]];
    [products addObject:[self initWithCategory:@"Pantalón Corto" client:@"Mujer" sport:@"Running" model:@"5cm Rival Printed" brand:@"Nike" price:40 image1URI:@"http://goo.gl/nyMWKx" image2URI:@"http://goo.gl/5SCsFF"]];
    [products addObject:[self initWithCategory:@"Falda" client:@"Mujer" sport:@"Tennis" model:@"Court" brand:@"Nike" price:46.97 image1URI:@"http://goo.gl/GMbo9S" image2URI:@"http://goo.gl/UWgnc4"]];
    [products addObject:[self initWithCategory:@"Falda" client:@"Mujer" sport:@"Tennis" model:@"Victory Printed" brand:@"Nike" price:46.97 image1URI:@"http://goo.gl/oVrCrI" image2URI:@"http://goo.gl/8djemA"]];
    [products addObject:[self initWithCategory:@"Vestido" client:@"Mujer" sport:@"Golf" model:@"Innovation Links" brand:@"Nike" price:163.85 image1URI:@"http://goo.gl/yNSe7l" image2URI:@"http://goo.gl/7A7x2g"]];
    [products addObject:[self initWithCategory:@"Vestido" client:@"Mujer" sport:@"Tennis" model:@"Premier Maria" brand:@"Nike" price:163.85 image1URI:@"http://goo.gl/RdKiAj" image2URI:@"http://goo.gl/gvHRiq"]];
    return products;
}

-(NSMutableArray *)getClothesChildProducts{
    NSMutableArray *products = [[NSMutableArray alloc] init];
    [products addObject:[self initWithCategory:@"Camiseta" client:@"Niños" sport:@"Sportwear" model:@"HBR" brand:@"Nike" price:18.99 image1URI:@"http://goo.gl/oXweiz" image2URI:@"http://goo.gl/RwiwYZ"]];
    [products addObject:[self initWithCategory:@"Camiseta" client:@"Niños" sport:@"Fútbol" model:@"FFF Squad Short-Sleeve Training 2" brand:@"Nike" price:32.77 image1URI:@"http://goo.gl/EpAaib" image2URI:@"http://goo.gl/rDRAfF"]];
    [products addObject:[self initWithCategory:@"Camiseta" client:@"Niños" sport:@"Sportwear" model:@"Dash J" brand:@"Nike" price:13.99 image1URI:@"http://goo.gl/c18bPn" image2URI:@"http://goo.gl/g9ukSa"]];
    [products addObject:[self initWithCategory:@"Camiseta" client:@"Niños" sport:@"Running" model:@"Dri-FIT Cool 2-in-1 Cami" brand:@"Nike" price:27.99 image1URI:@"http://goo.gl/Vz2rNP" image2URI:@"http://goo.gl/AcNSwq"]];
    [products addObject:[self initWithCategory:@"Sudadera con capucha" client:@"Niños" sport:@"Running" model:@"Tech Fleece Full-Zip" brand:@"Nike" price:60.08 image1URI:@"http://goo.gl/survkj" image2URI:@"http://goo.gl/ovbv81"]];
    [products addObject:[self initWithCategory:@"Sudadera con capucha" client:@"Niños" sport:@"Running" model:@"Tech Fleece Full-Zip Cape" brand:@"Nike" price:71 image1URI:@"http://goo.gl/t6qfz1" image2URI:@"http://goo.gl/qaZ5La"]];
    [products addObject:[self initWithCategory:@"Sudadera con capucha" client:@"Niños" sport:@"Sportwear" model:@"KO 2.0" brand:@"Nike" price:41.51 image1URI:@"http://goo.gl/gyqJBx" image2URI:@"http://goo.gl/E3n5jm"]];
    [products addObject:[self initWithCategory:@"Sudadera sin capucha" client:@"Niños" sport:@"Sportwear" model:@"YA76 HBR Crew" brand:@"Nike" price:43.69 image1URI:@"http://goo.gl/aIs7kY" image2URI:@"http://goo.gl/cjiYzW"]];
    [products addObject:[self initWithCategory:@"Pantalón" client:@"Niños" sport:@"Sportwear" model:@"HBR Cuffed" brand:@"Nike" price:38.23 image1URI:@"http://goo.gl/aUw5sF" image2URI:@"http://goo.gl/NWfGE1"]];
    [products addObject:[self initWithCategory:@"Pantalón" client:@"Niños" sport:@"Sportwear" model:@"N45 HBR Brushed Fleece Cuffed" brand:@"Nike" price:80.45 image1URI:@"http://goo.gl/NDju7e" image2URI:@"http://goo.gl/w8GfjM"]];
    [products addObject:[self initWithCategory:@"Licra" client:@"Niños" sport:@"Running" model:@"Allover Print" brand:@"Nike" price:27.31 image1URI:@"http://goo.gl/7I6Vi8" image2URI:@"http://goo.gl/b19AKP"]];
    [products addObject:[self initWithCategory:@"Licra" client:@"Niños" sport:@"Running" model:@"Legend 2.0 Allover Print Tight" brand:@"Nike" price:32.77 image1URI:@"http://goo.gl/hXz8jI" image2URI:@"http://goo.gl/Ci6S16"]];
    [products addObject:[self initWithCategory:@"Pantalón Corto" client:@"Niños" sport:@"Fútbol" model:@"2014 England Stadium Goalkeeper" brand:@"Nike" price:36.05 image1URI:@"http://goo.gl/qcWn1d" image2URI:@"http://goo.gl/n01sj9"]];
    [products addObject:[self initWithCategory:@"Pantalón Corto" client:@"Niños" sport:@"Baloncesto" model:@"Kobe Emerge Statement" brand:@"Nike" price:26.75 image1URI:@"http://goo.gl/Uzgz9C" image2URI:@"http://goo.gl/lPvxzM"]];
    [products addObject:[self initWithCategory:@"Pantalón Corto" client:@"Niños" sport:@"Running" model:@"Tempo Rival Graphic" brand:@"Nike" price:27.39 image1URI:@"http://goo.gl/wTgcIp" image2URI:@"http://goo.gl/TO2Vql"]];
    [products addObject:[self initWithCategory:@"Pantalón Corto" client:@"Niños" sport:@"Fútbol" model:@"2014/15 FC Barcelona Stadium" brand:@"Nike" price:36.15 image1URI:@"http://goo.gl/zUZ44Y" image2URI:@"http://goo.gl/8LVSk1"]];
    [products addObject:[self initWithCategory:@"Falda" client:@"Niños" sport:@"Tennis" model:@"Victory Power" brand:@"Nike" price:32.86 image1URI:@"http://goo.gl/KwhZnc" image2URI:@"http://goo.gl/AbqZwU"]];
    [products addObject:[self initWithCategory:@"Falda" client:@"Niños" sport:@"Golf" model:@"Knit" brand:@"Nike" price:76.68 image1URI:@"http://goo.gl/DRfg1e" image2URI:@"http://goo.gl/AITMnk"]];
    return products;
}

-(NSMutableArray *)getShoesMenProducts{
    NSMutableArray *products = [[NSMutableArray alloc] init];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Running" model:@"Flyknit Lunar 3 " brand:@"Nike" price:206.8 image1URI:@"http://goo.gl/hUcNZA" image2URI:@"http://goo.gl/JQsUHJ"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Running" model:@"Air Zoom Pegasus" brand:@"Nike" price:116.66 image1URI:@"http://goo.gl/nVoOYF" image2URI:@"http://goo.gl/AbFqvY"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Running" model:@"Free 4.0 Flyknit" brand:@"Nike" price:137.87 image1URI:@"http://goo.gl/RLxPNa" image2URI:@"http://goo.gl/WvuMWN"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Running" model:@"LunarEclipse 5" brand:@"Nike" price:159.07 image1URI:@"http://goo.gl/1C90QY" image2URI:@"http://goo.gl/bp6rZo"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Running" model:@"Zoom Terra Kiger" brand:@"Nike" price:148.47 image1URI:@"http://goo.gl/2R3jRd" image2URI:@"http://goo.gl/kixVsS"]];
    [products addObject:[self initWithCategory:@"Zapatos" client:@"Hombre" sport:@"Fútbol" model:@"Tiempo Legend V Premium FG" brand:@"Nike" price:238.61 image1URI:@"http://goo.gl/wxf17A" image2URI:@"http://goo.gl/4RB06s"]];
    [products addObject:[self initWithCategory:@"Zapatos" client:@"Hombre" sport:@"Fútbol" model:@"Mercurial Superfly FG" brand:@"Nike" price:291.64 image1URI:@"http://goo.gl/xIJRgU" image2URI:@"http://goo.gl/YSaQWw"]];
    [products addObject:[self initWithCategory:@"Zapatos" client:@"Hombre" sport:@"Fútbol" model:@"Magista Opus FG" brand:@"Nike" price:212.1 image1URI:@"http://goo.gl/ZUVWYX" image2URI:@"http://goo.gl/BxNNVi"]];
    [products addObject:[self initWithCategory:@"Zapatos" client:@"Hombre" sport:@"Fútbol" model:@"HYPERVENOM Phantom FG" brand:@"Nike" price:233.31 image1URI:@"http://goo.gl/q8fpkm" image2URI:@"http://goo.gl/RZiSV3"]];
    [products addObject:[self initWithCategory:@"Zapatos" client:@"Hombre" sport:@"Fútbol" model:@"Elastico Finale III TF" brand:@"Nike" price:84.84 image1URI:@"http://goo.gl/20jWMt" image2URI:@"http://goo.gl/5fB3MX"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Baloncesto" model:@"LeBron 12 AS" brand:@"Nike" price:190.89 image1URI:@"http://goo.gl/QHnNEg" image2URI:@"http://goo.gl/P6ZI1f"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Baloncesto" model:@"Zoom HyperRev 2015" brand:@"Nike" price:132.56 image1URI:@"http://goo.gl/1sYd6A" image2URI:@"http://goo.gl/j1jfwU"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Baloncesto" model:@"Blazer High Vintage ND" brand:@"Nike" price:106.05 image1URI:@"http://goo.gl/Or7nUp" image2URI:@"http://goo.gl/LFeMnR"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Baloncesto" model:@"Air Foamposite One" brand:@"Nike" price:233.31 image1URI:@"http://goo.gl/6jRy4C" image2URI:@"http://goo.gl/0LcB9A"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Baloncesto" model:@"Chuck Posite" brand:@"Nike" price:212.1 image1URI:@"http://goo.gl/HSCPQL" image2URI:@"http://goo.gl/Tol1Jb"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Tennis" model:@"Zoom Cage 2" brand:@"Nike" price:121.87 image1URI:@"http://goo.gl/rkpzDK" image2URI:@"http://goo.gl/2bhb4l"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Tennis" model:@"Zoom Vapor 9.5 Tour" brand:@"Nike" price:137.76 image1URI:@"http://goo.gl/FZA8cS" image2URI:@"http://goo.gl/Q9vknp"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Tennis" model:@"Lunar Ballistec 1.5" brand:@"Nike" price:169.55 image1URI:@"http://goo.gl/STh2tv" image2URI:@"http://goo.gl/Ncv2So"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Tennis" model:@"Dual Fusion Ballistec Advantage" brand:@"Nike" price:95.37 image1URI:@"http://goo.gl/sF5NBv" image2URI:@"http://goo.gl/GUZUR4"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Tennis" model:@"Air Vapor Advantage" brand:@"Nike" price:84.78 image1URI:@"http://goo.gl/PEzLHB" image2URI:@"http://goo.gl/A0T6BU"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Sportwear" model:@"Tennis Classic AC Premium" brand:@"Nike" price:116.57 image1URI:@"http://goo.gl/x6b2hN" image2URI:@"http://goo.gl/IsdQFT"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Sportwear" model:@"Roshe Flyknit" brand:@"Nike" price:137.76 image1URI:@"http://goo.gl/MwJMwH" image2URI:@"http://goo.gl/XHoXoP"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Sportwear" model:@"Air Huarache" brand:@"Nike" price:127.16 image1URI:@"http://goo.gl/HcNVHz" image2URI:@"http://goo.gl/E2sjFn"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Sportwear" model:@"Air Wildwood Premium RT" brand:@"Nike" price:132.46 image1URI:@"http://goo.gl/ayJvi8" image2URI:@"http://goo.gl/Z6tHqy"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Hombre" sport:@"Sportwear" model:@"Air Max 1 Essential" brand:@"Nike" price:116.57 image1URI:@"http://goo.gl/bljKug" image2URI:@"http://goo.gl/pyc7fK"]];
    [products addObject:[self initWithCategory:@"Botas" client:@"Hombre" sport:@"Sportwear" model:@"SFB Field 15cm" brand:@"Nike" price:222.54 image1URI:@"http://goo.gl/zZ9ORk" image2URI:@"http://goo.gl/V4fbzt"]];
    return products;
}

-(NSMutableArray *)getShoesWomenProducts{
    NSMutableArray *products = [[NSMutableArray alloc] init];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Mujer" sport:@"Running" model:@"Air Max 2015" brand:@"Nike" price:204.61 image1URI:@"http://goo.gl/NCkBtq" image2URI:@"http://goo.gl/FLQ56T"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Mujer" sport:@"Running" model:@"Air Zoom Vomero 9" brand:@"Nike" price:150.77 image1URI:@"http://goo.gl/z39BP9" image2URI:@"http://goo.gl/uNXijp"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Mujer" sport:@"Running" model:@"Lunaracer+ 3" brand:@"Nike" price:110.92 image1URI:@"http://goo.gl/5Nla8O" image2URI:@"http://goo.gl/yZH0h6"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Mujer" sport:@"Tennis" model:@"Zoom Cage 2" brand:@"Nike" price:123.84 image1URI:@"http://goo.gl/y1vcoh" image2URI:@"http://goo.gl/JleLEn"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Mujer" sport:@"Tennis" model:@"Vapor Court" brand:@"Nike" price:59.23 image1URI:@"http://goo.gl/uLgSxu" image2URI:@"http://goo.gl/OKL4vI"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Mujer" sport:@"Tennis" model:@"Lunar Ballistec 1.5" brand:@"Nike" price:172.3 image1URI:@"http://goo.gl/sKcusX" image2URI:@"http://goo.gl/7R1YR4"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Mujer" sport:@"Sportwear" model:@"Air Max Thea" brand:@"Nike" price:129.23 image1URI:@"http://goo.gl/8JoRjg" image2URI:@"http://goo.gl/VaF0Ae"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Mujer" sport:@"Sportwear" model:@"Air Max 1 Ultra LOTC (New York)" brand:@"Nike" price:204.61 image1URI:@"http://goo.gl/qw11o6" image2URI:@"http://goo.gl/9YRNQn"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Mujer" sport:@"Sportwear" model:@"Dunk High Skinny Premium" brand:@"Nike" price:113.07 image1URI:@"http://goo.gl/aXJuZM" image2URI:@"http://goo.gl/oBtkES"]];
    [products addObject:[self initWithCategory:@"Botas" client:@"Mujer" sport:@"Sportwear" model:@"Roshe Run Hi SneakerBoot" brand:@"Nike" price:126 image1URI:@"http://goo.gl/NX6POE" image2URI:@"http://goo.gl/1h333T"]];
    [products addObject:[self initWithCategory:@"Sandalias" client:@"Mujer" sport:@"Sportwear" model:@"Lunarsandiator Sky Hi" brand:@"Nike" price:115.23 image1URI:@"http://goo.gl/kKDzMD" image2URI:@"http://goo.gl/1tamt2"]];
    [products addObject:[self initWithCategory:@"Sandalias" client:@"Mujer" sport:@"Sportwear" model:@"Solarsoft II Print" brand:@"Nike" price:15.08 image1URI:@"http://goo.gl/ZZF22h" image2URI:@"http://goo.gl/ycvqnI"]];
    return products;
}

-(NSMutableArray *)getShoesChildProducts{
    NSMutableArray *products = [[NSMutableArray alloc] init];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Niños" sport:@"Running" model:@"Free 5.0" brand:@"Nike" price:92.8 image1URI:@"http://goo.gl/SXdSnS" image2URI:@"http://goo.gl/KmCmQb"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Niños" sport:@"Running" model:@"Free 5.0 Hypervenom" brand:@"Nike" price:92.8 image1URI:@"http://goo.gl/dMlTzM" image2URI:@"http://goo.gl/J1302y"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Niños" sport:@"Running" model:@"Air Zoom Pegasus 31" brand:@"Nike" price:76.43 image1URI:@"http://goo.gl/vFrXCy" image2URI:@"http://goo.gl/nbk3eN"]];
    [products addObject:[self initWithCategory:@"Zapatos" client:@"Niños" sport:@"Fútbol" model:@"Jr. HYPERVENOM Phade FG-R" brand:@"Nike" price:49.13 image1URI:@"http://goo.gl/4k7tl1" image2URI:@"http://goo.gl/xYQYbq"]];
    [products addObject:[self initWithCategory:@"Zapatos" client:@"Niños" sport:@"Fútbol" model:@"Jr. Hypervenom Phelon Premium IC 'Liquid Diamond'" brand:@"Nike" price:65.51 image1URI:@"http://goo.gl/mDczru" image2URI:@"http://goo.gl/UpUazs"]];
    [products addObject:[self initWithCategory:@"Zapatos" client:@"Niños" sport:@"Fútbol" model:@"Jr. Tiempo Genio Leather FG" brand:@"Nike" price:51.31 image1URI:@"http://goo.gl/l2eqOz" image2URI:@"http://goo.gl/0CPJhT"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Niños" sport:@"Baloncesto" model:@"Kobe X" brand:@"Nike" price:120.1 image1URI:@"http://goo.gl/FG4kEC" image2URI:@"http://goo.gl/2Luj5b"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Niños" sport:@"Baloncesto" model:@"Jordan Super.Fly 3" brand:@"Nike" price:109.18 image1URI:@"http://goo.gl/CTHX79" image2URI:@"http://goo.gl/YCpnCy"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Niños" sport:@"Baloncesto" model:@"Air Jordan 1 Retro High OG Laser" brand:@"Nike" price:163.77 image1URI:@"http://goo.gl/jWKjjL" image2URI:@"http://goo.gl/GXHJkW"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Niños" sport:@"Sportwear" model:@"Roshe Run" brand:@"Nike" price:70.97 image1URI:@"http://goo.gl/NJpI77" image2URI:@"http://goo.gl/1i4MOZ"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Niños" sport:@"Sportwear" model:@"Air Max 90" brand:@"Nike" price:100 image1URI:@"http://goo.gl/Wg55XC" image2URI:@"http://goo.gl/aOb7FO"]];
    [products addObject:[self initWithCategory:@"Zapatillas" client:@"Niños" sport:@"Sportwear" model:@"Court Tradition 2 Plus" brand:@"Nike" price:38.21 image1URI:@"http://goo.gl/imeAkf" image2URI:@"http://goo.gl/oRRYi5"]];
    [products addObject:[self initWithCategory:@"Sandalias" client:@"Niños" sport:@"Sportwear" model:@"Benassi Just Do It" brand:@"Nike" price:25.11 image1URI:@"http://goo.gl/FVO4oK" image2URI:@"http://goo.gl/LU0m9F"]];
    return products;
}

-(NSMutableArray *)getAccessoryProducts{
    NSMutableArray *products = [[NSMutableArray alloc] init];
    [products addObject:[self initWithCategory:@"Gorra" client:@"" sport:@"Sportwear" model:@"AW84 Internationalist" brand:@"Nike" price:27.39 image1URI:@"http://goo.gl/66Y6kK" image2URI:@"http://goo.gl/uCMqAy"]];
    [products addObject:[self initWithCategory:@"Gorra" client:@"" sport:@"Fútbol" model:@"England AW84" brand:@"Nike" price:25.19 image1URI:@"http://goo.gl/3iU0Wc" image2URI:@"http://goo.gl/Dp4tG0"]];
    [products addObject:[self initWithCategory:@"Bolsa" client:@"" sport:@"Tennis" model:@"Air Max Vapor" brand:@"Nike" price:49.29 image1URI:@"http://goo.gl/qzOUQY" image2URI:@"http://goo.gl/VwUAGd"]];
    [products addObject:[self initWithCategory:@"Bolsa" client:@"" sport:@"Baloncesto" model:@"Sport II" brand:@"Nike" price:50.67 image1URI:@"http://goo.gl/HLg8sR" image2URI:@"http://goo.gl/zb0pnh"]];
    [products addObject:[self initWithCategory:@"Mochila" client:@"" sport:@"Running" model:@"Shield Standard" brand:@"Nike" price:49.29 image1URI:@"http://goo.gl/J8J5hF" image2URI:@"http://goo.gl/rnZrnn"]];
    [products addObject:[self initWithCategory:@"Mochila" client:@"" sport:@"Fútbol" model:@"FFF Allegiance 2.0" brand:@"Nike" price:12.49 image1URI:@"http://goo.gl/88i3Qa" image2URI:@"http://goo.gl/eWSUY9"]];
    [products addObject:[self initWithCategory:@"Bolso" client:@"" sport:@"Sportwear" model:@"Victory Gym Club" brand:@"Nike" price:90 image1URI:@"http://goo.gl/jvztw4" image2URI:@"http://goo.gl/xyfzgN"]];
    [products addObject:[self initWithCategory:@"Bolso" client:@"" sport:@"Sportwear" model:@"Heritage SI Club" brand:@"Nike" price:45 image1URI:@"http://goo.gl/bG23GA" image2URI:@"http://goo.gl/5l1q6e"]];
    [products addObject:[self initWithCategory:@"Calcetines" client:@"" sport:@"Running" model:@"Nike Anti-Blister Lightweight Low-Cut" brand:@"Nike" price:8 image1URI:@"http://goo.gl/i9aA2T" image2URI:@"http://goo.gl/s7x085"]];
    [products addObject:[self initWithCategory:@"Calcetines" client:@"" sport:@"Fútbol" model:@"2014/15 AS Roma Stadium OTC" brand:@"Nike" price:15 image1URI:@"http://goo.gl/VDnOYp" image2URI:@"http://goo.gl/yB453q"]];
    [products addObject:[self initWithCategory:@"Canilleras" client:@"" sport:@"Fútbol" model:@"Hard Shell Slip-In" brand:@"Nike" price:19.67 image1URI:@"http://goo.gl/9cyf8t" image2URI:@""]];
    [products addObject:[self initWithCategory:@"Canilleras" client:@"" sport:@"Fútbol" model:@"Protegga Shield" brand:@"Nike" price:27.8 image1URI:@"http://goo.gl/KWt0tH" image2URI:@""]];
    [products addObject:[self initWithCategory:@"Guantes" client:@"" sport:@"Fútbol" model:@"GK Classic" brand:@"Nike" price:21.91 image1URI:@"http://goo.gl/ePbNEc" image2URI:@"http://goo.gl/ug9IXH"]];
    [products addObject:[self initWithCategory:@"Guantes" client:@"" sport:@"Fútbol" model:@"Hyperwarm Field Player's" brand:@"Nike" price:25.19 image1URI:@"http://goo.gl/KL8xqP" image2URI:@"http://goo.gl/Jg4FG4"]];
    [products addObject:[self initWithCategory:@"Bufandas" client:@"" sport:@"Fútbol" model:@"Paris Saint-Germain Supporters" brand:@"Nike" price:27.39 image1URI:@"http://goo.gl/ffLjpm" image2URI:@""]];
    [products addObject:[self initWithCategory:@"Bufandas" client:@"" sport:@"Fútbol" model:@"FC Barcelona Double Stripe Bufanda" brand:@"Nike" price:20.81 image1URI:@"http://goo.gl/YJ7ZQv" image2URI:@""]];
    [products addObject:[self initWithCategory:@"Balones" client:@"" sport:@"Fútbol" model:@"Nike LFP" brand:@"Nike" price:15.7 image1URI:@"http://goo.gl/a4WQZ6" image2URI:@""]];
    [products addObject:[self initWithCategory:@"Balones" client:@"" sport:@"Baloncesto" model:@"Jordan Legacy" brand:@"Nike" price:19.71 image1URI:@"http://goo.gl/ZeaY8r" image2URI:@""]];
    [products addObject:[self initWithCategory:@"Cinta de Cabello" client:@"" sport:@"Tennis" model:@"DRI-FIT" brand:@"Nike" price:9 image1URI:@"http://goo.gl/z7kdGp" image2URI:@""]];
    [products addObject:[self initWithCategory:@"Visera" client:@"" sport:@"Tennis" model:@"Featherlight 2.0" brand:@"Nike" price:18 image1URI:@"http://goo.gl/907Ukc" image2URI:@"http://goo.gl/g8Zec3"]];
    [products addObject:[self initWithCategory:@"Muñequeras" client:@"" sport:@"Tennis" model:@"DRI-FIT" brand:@"Nike" price:11 image1URI:@"http://goo.gl/plG1by" image2URI:@""]];
    return products;
}

-(NSMutableDictionary *)getBarcodeProducts{
    NSMutableDictionary *products = [[NSMutableDictionary alloc] init];
    
    [products setObject:[self initWithCategory:@"Bolsa" client:@"" sport:@"Baloncesto" model:@"Sport II" brand:@"Nike" price:50.67 image1URI:@"http://goo.gl/HLg8sR" image2URI:@"http://goo.gl/zb0pnh"] forKey:@"0887231756636"];
    [products setObject:[self initWithCategory:@"Sudadera sin capucha" client:@"Niños" sport:@"Sportwear" model:@"YA76 HBR Crew" brand:@"Nike" price:43.69 image1URI:@"http://goo.gl/aIs7kY" image2URI:@"http://goo.gl/cjiYzW"] forKey:@"4052557899411"];
    [products setObject:[self initWithCategory:@"Zapatillas" client:@"Mujer" sport:@"Sportwear" model:@"Air Max Thea" brand:@"Nike" price:129.23 image1URI:@"http://goo.gl/8JoRjg" image2URI:@"http://goo.gl/VaF0Ae"] forKey:@"0885176554713"];
    [products setObject:[self initWithCategory:@"Zapatillas" client:@"Niños" sport:@"Running" model:@"Air Zoom Pegasus 31" brand:@"Nike" price:76.43 image1URI:@"http://goo.gl/vFrXCy" image2URI:@"http://goo.gl/nbk3eN"] forKey:@"4054712576403"];
    [products setObject:[self initWithCategory:@"Pantalón Corto" client:@"Mujer" sport:@"Fútbol" model:@"Gameday Woven" brand:@"Nike" price:25 image1URI:@"http://goo.gl/hzlMK0" image2URI:@"http://goo.gl/f3r8Uy"] forKey:@"4053518963806"];
    [products setObject:[self initWithCategory:@"Sudadera sin capucha" client:@"Hombre" sport:@"Sportwear" model:@"F.C. AW77 Long-Sleeve Crew" brand:@"Nike" price:71 image1URI:@"http://goo.gl/kxXO1j" image2URI:@"http://goo.gl/Q5l7eV"] forKey:@"4053518503996"];
    
    
    
    return products;
}

@end
