//
//  SCProduct.h
//  iSportCatalog
//
//  Created by Paul017 on 23/3/15.
//
//

#import <Foundation/Foundation.h>

@interface SCProduct : NSObject

@property (nonatomic) int productID;
@property (nonatomic) NSString *productClass;
@property (nonatomic) NSString *productCategory;
@property (nonatomic) NSString *productModel;
@property (nonatomic) NSString *productState;
@property (nonatomic) NSString *productBrand;
@property (nonatomic) NSString *productClient;
@property (nonatomic) NSString *productSport;
@property (nonatomic) float productPrice;
@property (nonatomic) NSString *productPrimaryImageURI;
@property (nonatomic) NSString *productSecondaryImageURI;
@property (nonatomic) NSString *productSize;
@property (nonatomic) NSString *productBarcode;

-(SCProduct *)initWithID:(int) productID class:(NSString*)class category:(NSString *)category client:(NSString *)client sport:(NSString *)sport model:(NSString *)model brand:(NSString *)brand price:(float)price state:(NSString*)state image1URI:(NSString *)image1URI image2URI:(NSString *)image2URI andSize:(NSString*)size;

//FIXME
-(NSMutableArray *)getOutstandingProducts;
-(NSMutableArray *)getClothesMenProducts;
-(NSMutableArray *)getClothesWomenProducts;
-(NSMutableArray *)getClothesChildProducts;
-(NSMutableArray *)getShoesMenProducts;
-(NSMutableArray *)getShoesWomenProducts;
-(NSMutableArray *)getShoesChildProducts;
-(NSMutableArray *)getAccessoryProducts;
-(NSMutableDictionary *)getBarcodeProducts;

@end
