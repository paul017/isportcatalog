//
//  SCScanViewController.m
//  iSportCatalog
//
//  Created by Paul017 on 19/2/15.
//
//

#import "SCScanViewController.h"
#import "SCuiConstants.h"
#import "SCuiSingleton.h"
#import "SCProduct.h"
#import "SCContentProtocol.h"
#import <AVFoundation/AVFoundation.h>

@interface SCScanViewController ()<AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic) id<SCContentProtocol> contentFacade;

@property (nonatomic) SCuiSingleton *uiSingleton;
@property (weak, nonatomic) IBOutlet UIView *scanView;
@property (nonatomic) SCProduct *product;
@property (nonatomic) NSMutableDictionary *barcodeProducts;
@property (weak, nonatomic) IBOutlet UITextField *barcodeTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *scanActivityIndicatorView;

@property AVCaptureSession *session;
@property AVCaptureDevice *device;
@property AVCaptureDeviceInput *input;
@property AVCaptureMetadataOutput *output;
@property AVCaptureVideoPreviewLayer *prevLayer;
@property UIView *highlightView;

@end

@implementation SCScanViewController

objection_requires(@"uiSingleton",@"contentFacade")

@synthesize contentFacade = _contentFacade;
@synthesize uiSingleton = _uiSingleton;

@synthesize session = _session;
@synthesize device = _device;
@synthesize input = _input;
@synthesize output = _output;
@synthesize prevLayer = _prevLayer;
@synthesize highlightView = _highlightView;
@synthesize product = _product;
@synthesize barcodeProducts = _barcodeProducts;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[JSObjection defaultInjector] injectDependencies:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showProductDetail) name:kShowProductDetail object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMessageError) name:kShowErrorServerMessage object:nil];
    _product = [[SCProduct alloc] init];
    _barcodeProducts = [_product getBarcodeProducts];
    
    self.scanActivityIndicatorView.hidesWhenStopped = YES;
    _highlightView = [[UIView alloc] init];
    _highlightView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
    _highlightView.layer.borderColor = [UIColor greenColor].CGColor;
    _highlightView.layer.borderWidth = 3;
    [self.view addSubview:_highlightView];
    
    _session = [[AVCaptureSession alloc] init];
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
    if (_input) {
        [_session addInput:_input];
    } else {
        NSLog(@"Error: %@", error);
    }
    
    _output = [[AVCaptureMetadataOutput alloc] init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [_session addOutput:_output];
    
    _output.metadataObjectTypes = [_output availableMetadataObjectTypes];
    
    _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    _prevLayer.frame = self.scanView.frame;
    _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:_prevLayer];
    
    [_session startRunning];
    
    [self.view bringSubviewToFront:_highlightView];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [_session startRunning];
    [self.barcodeTextField setText:@""];
    if (_uiSingleton.isSlideMenuEnabled) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kSlideMenuViewEnabled object:nil userInfo:@{kIsSlideMenuViewEnabled: @NO}];
        _uiSingleton.isSlideMenuEnabled = NO;
    }
    
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    CGRect highlightViewRect = CGRectZero;
    AVMetadataMachineReadableCodeObject *barCodeObject;
    NSString *detectionString = nil;
    NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];
    
    for (AVMetadataObject *metadata in metadataObjects) {
        for (NSString *type in barCodeTypes) {
            if ([metadata.type isEqualToString:type])
            {
                barCodeObject = (AVMetadataMachineReadableCodeObject *)[_prevLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                highlightViewRect = CGRectMake
                (barCodeObject.bounds.origin.x + self.scanView.frame.origin.x,
                 barCodeObject.bounds.origin.y + self.scanView.frame.origin.y,
                 barCodeObject.bounds.size.width,
                 barCodeObject.bounds.size.height);
                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
                break;
            }
        }
        
        if (detectionString != nil)
        {
            [self.barcodeTextField setText:detectionString];
            [self captureBarcode:detectionString];
            self.scanActivityIndicatorView.hidden = false;
            [self.scanActivityIndicatorView startAnimating];
            [_session stopRunning];
            break;
        }
        else
            [self.barcodeTextField setText:kDefaultScanText];
    }
    
    _highlightView.frame = highlightViewRect;
}

#pragma mark - Server Connection

-(void) captureBarcode:(NSString*)barcode{
    [_contentFacade requestServerProductByBarcode:barcode];
}

-(void)showProductDetail{
    [self.scanActivityIndicatorView stopAnimating];
    [self performSegueWithIdentifier:kScanToProductDetailSegue sender:self];
}

-(void)showMessageError{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lo sentimos"
                                                    message:@"No encontramos tu producto"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (buttonIndex == 0) {
        [self.scanActivityIndicatorView stopAnimating];
        [_session startRunning];
        [self.barcodeTextField setText:@""];
    }
}

#pragma mark - Navigation Segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UIViewController *destinationVC = segue.destinationViewController;
    destinationVC.navigationItem.title = _uiSingleton.productSelected.productCategory;
}
@end
