//
//  SCContentProtocol.h
//  iSportCatalog
//
//  Created by Paul017 on 9/8/15.
//
//

#import <Foundation/Foundation.h>
#import "SCRemoteContentDelegate.h"

@protocol SCContentProtocol <NSObject>

-(void) requestServerProducts:(NSString*) categoryFind;
-(void) requestServerProductByBarcode:(NSString*) barcode;

@end
