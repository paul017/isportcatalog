//
//  SCContentFacade.h
//  iSportCatalog
//
//  Created by Paul017 on 9/8/15.
//
//

#import <Foundation/Foundation.h>
#import "SCContentProtocol.h"
#import "SCRemoteContentDelegate.h"
#import "SCRemoteContentProtocol.h"
#import "SCuiSingleton.h"

@interface SCContentFacade : NSObject <SCRemoteContentDelegate, SCContentProtocol>

@property (nonatomic) int attemptServerConnection;

// Injected
@property(nonatomic) id <SCRemoteContentProtocol> remoteContentService;
@property(nonatomic) SCuiSingleton *uiSingleton;

@end
