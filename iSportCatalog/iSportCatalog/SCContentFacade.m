//
//  SCContentFacade.m
//  iSportCatalog
//
//  Created by Paul017 on 9/8/15.
//
//

#import "SCContentFacade.h"
#import "SCuiConstants.h"

@implementation SCContentFacade

objection_requires(@"remoteContentService", @"uiSingleton")
objection_register_singleton(SCContentFacade)
@synthesize remoteContentService = _remoteContentService;
@synthesize uiSingleton = _uiSingleton;

- (id)init
{
    self = [super init];
    [[JSObjection defaultInjector] injectDependencies:self];
    self.attemptServerConnection = 0;
    return self;
}

-(void)requestServerProducts:(NSString *)categoryFind{
    [_remoteContentService requestServerProductsWithCategoryFind:categoryFind andDelegate:self];
}


-(void)productsReturned:(NSMutableArray *)products byCategoryFind:(NSString *)categoryFind{
    if ([products count]==0) {
        if (self.attemptServerConnection < 3) {
            [self requestServerProducts:categoryFind];
            self.attemptServerConnection++;
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowServerArticlesError" object:nil];
            self.attemptServerConnection = 0;
        }
    } else{
        _uiSingleton.productItems = products;
        [[NSNotificationCenter defaultCenter] postNotificationName:kReloadCarouselItems object:nil];
        self.attemptServerConnection = 0;
    }
}

-(void)productsRequestError:(NSError *)error{
    NSLog(@"Error: %@", error);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowServerArticlesError" object:nil];
}

-(void)requestServerProductByBarcode:(NSString *)barcode{
    [_remoteContentService requestServerProductByBarcode:barcode andDelegate:self];
}

-(void)productByBarcodeReturned:(SCProduct *)product{
    _uiSingleton.productSelected = product;
    if (product.productModel != nil)
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowProductDetail object:nil];
}

-(void)productByBArcodeRequestError:(NSString *)error{
    NSLog(@"Error: %@", error);
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowErrorServerMessage object:nil];
}

@end
