//
//  SCCategoryProduct.m
//  iSportCatalog
//
//  Created by Paul017 on 7/3/15.
//
//

#import "SCCategoryProduct.h"

@implementation SCCategoryProduct

@synthesize categoryProductName = _categoryProductName;
@synthesize subcategoryProductName = _subcategoryProductName;
@synthesize isSelectableProductCategory = _isSelectableProductCategory;

-(SCCategoryProduct *)initWithCategoryName:(NSString *)categoryName subcategoryName:(NSString *)subcategoryName andIsSelectableCategory:(BOOL)isSelectableCategory{
    _categoryProductName = categoryName;
    _subcategoryProductName = subcategoryName;
    _isSelectableProductCategory = isSelectableCategory;
    return self;
}

@end
