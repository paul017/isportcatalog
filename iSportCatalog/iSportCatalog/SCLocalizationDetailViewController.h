//
//  SCLocalizationDetailViewController.h
//  iSportCatalog
//
//  Created by Paul017 on 20/2/15.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface SCLocalizationDetailViewController : UIViewController <MKMapViewDelegate,CLLocationManagerDelegate>

@property (nonatomic, strong) IBOutlet MKMapView *mapView;

@end
