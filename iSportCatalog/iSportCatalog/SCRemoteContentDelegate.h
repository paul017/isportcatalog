//
//  SCRemoteContentDelegate.h
//  iSportCatalog
//
//  Created by Paul017 on 9/8/15.
//
//

#import <Foundation/Foundation.h>
#import "SCProduct.h"

@protocol SCRemoteContentDelegate <NSObject>

-(void) productsReturned:(NSMutableArray *)products byCategoryFind:(NSString*)categoryFind;
-(void) productsRequestError:(NSError *) error;

-(void) productByBarcodeReturned:(SCProduct *)product;
-(void) productByBArcodeRequestError:(NSString *)error;
@end
