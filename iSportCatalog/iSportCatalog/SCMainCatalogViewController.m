//
//  SCMainCatalogViewController.m
//  iSportCatalog
//
//  Created by Paul017 on 19/2/15.
//
//

#import "SCMainCatalogViewController.h"
#import "SCuiConstants.h"
#import "SCuiSingleton.h"
#import "FXImageView.h"
#import "SCProduct.h"
#import "PopoverView.h"
#import "SCContentProtocol.h"

@interface SCMainCatalogViewController ()

@property (nonatomic) id<SCContentProtocol> contentFacade;

@property (nonatomic, strong) UIBarButtonItem *paneRevealLeftBarButtonItem;
@property (nonatomic) SCuiSingleton *uiSingleton;
@property (weak, nonatomic) IBOutlet iCarousel *carousel;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic) NSMutableArray *allItems;
@property (nonatomic) SCProduct *product;
@property (nonatomic) UIButton *brandFilterButton;
@property (nonatomic) IBOutlet UIBarButtonItem *brandFilterItem;
@property (nonatomic) PopoverView *popoverView;
@property (nonatomic) NSString *currentFilterSelected;


@end

@implementation SCMainCatalogViewController

objection_requires(@"uiSingleton", @"contentFacade")

@synthesize contentFacade = _contentFacade;

@synthesize uiSingleton = _uiSingleton;
@synthesize paneRevealLeftBarButtonItem = _paneRevealLeftBarButtonItem;
@synthesize carousel = _carousel;
@synthesize items = _items;
@synthesize allItems = _allItems;
@synthesize product = _product;
@synthesize currentFilterSelected = _currentFilterSelected;

- (void)viewDidLoad {
    
    [super viewDidLoad];

    self.brandFilterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.brandFilterButton addTarget:self action:@selector(brandFilterSelected:) forControlEvents:UIControlEventTouchUpInside];
    [self.brandFilterButton setTitle:kDefautBrandText forState:UIControlStateNormal];
    [self.brandFilterButton setTitleColor:SC_BLUE_COLOR forState:UIControlStateNormal];
    self.brandFilterButton.titleLabel.font = [UIFont fontWithName:kGeneralFontName size:15.f];
    CGSize textSize = [kDefautBrandText sizeWithAttributes:@{NSFontAttributeName:self.brandFilterButton.titleLabel.font}];
    self.brandFilterButton.frame = CGRectMake(0, 0, textSize.width, textSize.height);
    [self.brandFilterItem setCustomView:self.brandFilterButton];
    
    [self setUpEmptyNavigationItem];
    [[JSObjection defaultInjector] injectDependencies:self];
    _carousel.type = iCarouselTypeCoverFlow2;

    [self setUpNotificationCenter];
    _currentFilterSelected = @"outstanding";
    [_contentFacade requestServerProducts:_currentFilterSelected];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    
    if (!_uiSingleton.isSlideMenuEnabled) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kSlideMenuViewEnabled object:nil userInfo:@{kIsSlideMenuViewEnabled: @YES}];
        _uiSingleton.isSlideMenuEnabled = YES;
    }

}

#pragma mark - Server Connection
-(void)reloadCarrouselItems{
    _items = _uiSingleton.productItems;
    _allItems = [_items mutableCopy];
    [_carousel reloadData];
}

-(void)setUpCategoryItems{
    if ([_uiSingleton.categoryProductSelected.categoryProductName isEqualToString:kClothesCategory]) {
        if ([_uiSingleton.categoryProductSelected.subcategoryProductName isEqualToString:kMenSubcategory]) {
            if (![_currentFilterSelected isEqualToString:@"menclothes"]) {
                _currentFilterSelected = @"menclothes";
                [_contentFacade requestServerProducts:@"menclothes"];
            }
        } else if ([_uiSingleton.categoryProductSelected.subcategoryProductName isEqualToString:kWomenSubcategory]){
            if (![_currentFilterSelected isEqualToString:@"womenclothes"]) {
                _currentFilterSelected = @"womenclothes";
                [_contentFacade requestServerProducts:@"womenclothes"];
            }
        } else {
            if (![_currentFilterSelected isEqualToString:@"childrenclothes"]) {
                _currentFilterSelected = @"childrenclothes";
                [_contentFacade requestServerProducts:@"childrenclothes"];
            }
        }
    } else if ([_uiSingleton.categoryProductSelected.categoryProductName isEqualToString:kFootwearCategory]) {
        if ([_uiSingleton.categoryProductSelected.subcategoryProductName isEqualToString:kMenSubcategory]) {
            if (![_currentFilterSelected isEqualToString:@"menshoes"]) {
                _currentFilterSelected = @"menshoes";
                [_contentFacade requestServerProducts:@"menshoes"];
            }
        } else if ([_uiSingleton.categoryProductSelected.subcategoryProductName isEqualToString:kWomenSubcategory]){
            if (![_currentFilterSelected isEqualToString:@"womenshoes"]) {
                _currentFilterSelected = @"womenshoes";
                [_contentFacade requestServerProducts:@"womenshoes"];
            }
        } else{
            if (![_currentFilterSelected isEqualToString:@"childrenshoes"]) {
                _currentFilterSelected = @"childrenshoes";
                [_contentFacade requestServerProducts:@"childrenshoes"];
            }
        }
    } else if ([_uiSingleton.categoryProductSelected.categoryProductName isEqualToString:kAccessoryCategory]) {
        if (![_currentFilterSelected isEqualToString:@"accessories"]) {
            _currentFilterSelected = @"accessories";
            [_contentFacade requestServerProducts:@"accessories"];
        }
    }else if ([_uiSingleton.categoryProductSelected.categoryProductName isEqualToString:kOutstandingCategory]) {
        if (![_currentFilterSelected isEqualToString:@"outstanding"]) {
            _currentFilterSelected = @"outstanding";
            [_contentFacade requestServerProducts:@"outstanding"];
        }
    }
    
    
    self.navigationItem.title = _uiSingleton.categoryProductSelected.categoryProductName;
    if (_carousel.currentItemIndex >= 0) {
        self.productNameLabel.text =[NSString stringWithFormat:@"%@ \n %@", [(SCProduct *)[_items objectAtIndex:_carousel.currentItemIndex] productCategory],[(SCProduct *)[_items objectAtIndex:_carousel.currentItemIndex] productModel]];
    }
    
    [self setAllBrandPopoverButton];
    //[_carousel reloadData];
}

-(void)showMessageError{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lo sentimos"
                                                    message:@"Los datos solicitados no están disponibles por el momento"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancelar"
                                          otherButtonTitles:@"Reintentar", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0){
        [_uiSingleton.productItems removeAllObjects];
        [self reloadCarrouselItems];
    }if (buttonIndex == 1){
        [_contentFacade requestServerProducts:_currentFilterSelected];
    }
   
}

#pragma mark - Custom Methods
-(void)setUpEmptyNavigationItem{
    if (!_paneRevealLeftBarButtonItem) {
        _paneRevealLeftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:kSlideMenuButtonImage] style:UIBarButtonItemStyleBordered target:self action:@selector(sendOpenMenuNotification)];
        self.navigationItem.leftBarButtonItem = _paneRevealLeftBarButtonItem;
    }
}

-(void)filterProductsForBrand: (NSString *)brand{
    if ([brand isEqualToString:kAllBrandsText]) {
        _items = [_allItems mutableCopy];
    } else{
        NSMutableArray *filteredProducts = [[NSMutableArray alloc] init];
        for (SCProduct *product in _allItems) {
            if ([product.productBrand isEqualToString:brand]) {
                [filteredProducts addObject:product];
            }
        }
        _items = [filteredProducts mutableCopy];
    }
    [_carousel reloadData];
}

#pragma mark - NSNotificationCenter
-(void)sendOpenMenuNotification{
    [[NSNotificationCenter defaultCenter] postNotificationName:kSlideMenuButtonTapped object:nil];
}

-(void)setUpNotificationCenter{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpCategoryItems) name:kSlideMenuItemChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadCarrouselItems) name:kReloadCarouselItems object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMessageError) name:@"ShowServerArticlesError" object:nil];
}

#pragma mark - iCarouselDataSource

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [_items count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    //create new view if no view is available for recycling
    if (view == nil)
    {
        FXImageView *imageView = [[FXImageView alloc] initWithFrame:CGRectMake(0, 0, 230.0f, 230.0f)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.asynchronous = YES;
        imageView.reflectionScale = 0.5f;
        imageView.reflectionAlpha = 0.25f;
        imageView.reflectionGap = 10.0f;
        imageView.shadowOffset = CGSizeMake(0.0f, 2.0f);
        imageView.shadowBlur = 5.0f;
        imageView.cornerRadius = 10.0f;
        view = imageView;
    }
    
    //show placeholder
    ((FXImageView *)view).processedImage = [UIImage imageNamed:kPlaceholderImage];
    
    //set image
    [(FXImageView *)view setImageWithContentsOfURL:[[NSURL alloc] initWithString:[(SCProduct *)[_items objectAtIndex:index] productPrimaryImageURI]]];
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return YES;
        }
        case iCarouselOptionTilt:
        {
            return 0.8;
        }
        case iCarouselOptionSpacing:
        {
            return value * 0.85;
        }
        default:
        {
            return value;
        }
    }
}

#pragma mark - iCarouselDelegate
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    _uiSingleton.productSelected = (SCProduct *) [_items objectAtIndex:index];
    _uiSingleton.productPrymaryImage = [(FXImageView *)carousel.currentItemView processedImage];
    [self performSegueWithIdentifier:kMainCatalogToProductSegue sender:self];
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.productNameLabel cache:YES];
    if (_carousel.currentItemIndex >= 0) {
        self.productNameLabel.text =[NSString stringWithFormat:@"%@ \n %@", [(SCProduct *)[_items objectAtIndex:carousel.currentItemIndex] productCategory],[(SCProduct *)[_items objectAtIndex:carousel.currentItemIndex] productModel]];
    } else {
        self.productNameLabel.text = nil;
    }
    
    [UIView commitAnimations];
}

#pragma mark - Popover View

//FIXME
#define kStringArray [NSArray arrayWithObjects:@"Todas", @"Adidas",@"Nike",@"Puma", nil]
#define kImageArray [NSArray arrayWithObjects:[UIImage imageNamed:@"logo_all"], [UIImage imageNamed:@"logo_adidas"],[UIImage imageNamed:@"logo_nike"],[UIImage imageNamed:@"logo_puma"], nil]

- (IBAction)brandFilterSelected:(id)sender {
    CGPoint point = CGPointMake((self.view.bounds.size.width - (self.brandFilterItem.width)/2),50);
    self.popoverView = [PopoverView showPopoverAtPoint:point
                                  inView:self.view
                               withTitle:kBrandPopoverTitle
                         withStringArray:kStringArray
                          withImageArray:kImageArray
                                delegate:self];
}



#pragma mark - PopoverViewDelegate Methods

- (void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index{
    
    
    // Figure out which string was selected, store in "string"
    NSString *string = [kStringArray objectAtIndex:index];
    
    // Show a success image, with the string from the array
    [popoverView showImage:[kImageArray objectAtIndex:index] withMessage:string];
    
    [self filterProductsForBrand:string];
    
    if (index == 0) {
        [self setAllBrandPopoverButton];
    } else {
        [self.brandFilterButton setTitle:nil forState:UIControlStateNormal];
        UIImage *imageLogo = [kImageArray objectAtIndex:index];
        [self.brandFilterButton setImage:imageLogo forState:UIControlStateNormal];
        self.brandFilterButton.frame = CGRectMake(0, 0, imageLogo.size.width, imageLogo.size.height);
    }
    
    // Dismiss the PopoverView after 0.5 seconds
    [popoverView performSelector:@selector(dismiss) withObject:nil afterDelay:0.5f];
}

- (void)popoverViewDidDismiss:(PopoverView *)popoverView
{
    self.popoverView = nil;
}

-(void) setAllBrandPopoverButton{
    [self.brandFilterButton setImage:nil forState:UIControlStateNormal];
    [self.brandFilterButton setTitle:kDefautBrandText forState:UIControlStateNormal];
    CGSize textSize = [kDefautBrandText sizeWithAttributes:@{NSFontAttributeName:self.brandFilterButton.titleLabel.font}];
    self.brandFilterButton.frame = CGRectMake(0, 0, textSize.width, textSize.height);
}

#pragma mark - Navigation Segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UIViewController *destinationVC = segue.destinationViewController;
    destinationVC.navigationItem.title = _uiSingleton.productSelected.productCategory;
}

@end
