//
//  SCProductDetailViewController.m
//  iSportCatalog
//
//  Created by Paul017 on 20/2/15.
//
//

#import "SCProductDetailViewController.h"
#import "SCuiSingleton.h"
#import "SCuiConstants.h"
#import "SCProductDetailCollectionViewCell.h"
#import "PopoverView.h"
#import "SCProductDetailTableViewCell.h"


@interface SCProductDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *transparentView;
@property (nonatomic) IBOutlet UITableView *infoTableView;
@property (nonatomic) SCuiSingleton *uiSingleton;
@property (weak, nonatomic) IBOutlet UIButton *moreInfoButton;
@property (nonatomic) PopoverView *popoverView;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;

@end

@implementation SCProductDetailViewController

objection_requires(@"uiSingleton")
@synthesize uiSingleton = _uiSingleton;
@synthesize productNameLabel = _productNameLabel;
@synthesize priceLabel = _priceLabel;
@synthesize transparentView = _transparentView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[JSObjection defaultInjector] injectDependencies:self];
    _productNameLabel.text = _uiSingleton.productSelected.productModel;
    _priceLabel.text = [NSString stringWithFormat:@"$ %.02f",_uiSingleton.productSelected.productPrice];
   self.infoTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 200, 212)];
   [self.infoTableView registerNib:[UINib nibWithNibName:kProductDetailTableViewCellNIB bundle:nil] forCellReuseIdentifier:kProductDetailInfoTableViewCell];
    self.infoTableView.dataSource = self;
    self.infoTableView.delegate = self;
    
    _transparentView.alpha = 0.0;
    _transparentView.hidden = YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    
    if (_uiSingleton.isSlideMenuEnabled) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kSlideMenuViewEnabled object:nil userInfo:@{kIsSlideMenuViewEnabled: @NO}];
        _uiSingleton.isSlideMenuEnabled = NO;
    }
    
}

#pragma mark - UICollectionViewDataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ([_uiSingleton.productSelected.productSecondaryImageURI length])
        return 2;
    else
        return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SCProductDetailCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kProductDetailCollectionViewCell forIndexPath:indexPath];

    cell.productImageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.productImageView.asynchronous = YES;
    cell.productImageView.reflectionScale = 0.5f;
    cell.productImageView.reflectionAlpha = 0.25f;
    cell.productImageView.reflectionGap = 10.0f;
    cell.productImageView.shadowOffset = CGSizeMake(0.0f, 2.0f);
    cell.productImageView.shadowBlur = 5.0f;
    cell.productImageView.cornerRadius = 10.0f;
    
    if (indexPath.row == 0) {
        [cell.productImageView setImageWithContentsOfURL:[[NSURL alloc] initWithString:_uiSingleton.productSelected.productPrimaryImageURI]];
    } else
        [cell.productImageView setImageWithContentsOfURL:[[NSURL alloc] initWithString:_uiSingleton.productSelected.productSecondaryImageURI]];
    //cell.productImageView.frame = CGRectMake(0, 0, 230.0f, 230.0f);
    return cell;
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SCProductDetailTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:kProductDetailInfoTableViewCell];
    switch (indexPath.row) {
        case 0:
            cell.titleLabel.text = kArticleNameText;
            cell.descriptionLabel.text = _uiSingleton.productSelected.productCategory;
            break;
        case 1:
            cell.titleLabel.text = kArticleModelText;
            cell.descriptionLabel.text = _uiSingleton.productSelected.productModel;
            break;
        case 2:
            cell.titleLabel.text = kArticleSportTex;
            cell.descriptionLabel.text = _uiSingleton.productSelected.productSport;
            break;
        case 3:
            cell.titleLabel.text = kArticleBrandText;
            cell.descriptionLabel.text = _uiSingleton.productSelected.productBrand;
            break;
        case 4:
            cell.titleLabel.text = kArticlePriceText;
            cell.descriptionLabel.text =  [NSString stringWithFormat:@"$ %.02f",_uiSingleton.productSelected.productPrice];
            break;
        case 5:
            if (_uiSingleton.productSelected.productSize) {
                cell.titleLabel.text = @"Talla:";
                cell.descriptionLabel.text = _uiSingleton.productSelected.productSize;
            } else {
                cell.titleLabel.text = @"";
                cell.descriptionLabel.text = _uiSingleton.productSelected.productSize;
            }
            break;
        default:
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;

}

#pragma mark - Info Button
- (IBAction)infoButtonPressed:(id)sender {
    [UIView animateWithDuration:0.5 animations:^{
        _transparentView.hidden = NO;
        _transparentView.alpha = 1.0;
    }];

    self.popoverView = [PopoverView showPopoverAtPoint:CGPointMake(self.moreInfoButton.center.x, self.moreInfoButton.center.y + (self.moreInfoButton.frame.size.height * 0.5))
                                  inView:self.view
                                withTitle:kInfoPopoverTitle
                         withContentView:self.infoTableView
                                delegate:self];
}

- (void)popoverViewDidDismiss:(PopoverView *)popoverView
{
    [UIView animateWithDuration:0.5 animations:^{
        _transparentView.alpha = 0.0;;
        _transparentView.hidden = YES;
    }];
    self.popoverView = nil;
    
}

@end
