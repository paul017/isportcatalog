//
//  SCMainCatalogViewController.h
//  iSportCatalog
//
//  Created by Paul017 on 19/2/15.
//
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "PopoverView.h"

@interface SCMainCatalogViewController : UIViewController <iCarouselDataSource, iCarouselDelegate, PopoverViewDelegate>

@end
