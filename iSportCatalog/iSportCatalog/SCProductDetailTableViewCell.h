//
//  SCProductDetailTableViewCell.h
//  iSportCatalog
//
//  Created by Paul017 on 27/6/15.
//
//

#import <UIKit/UIKit.h>

@interface SCProductDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@end
