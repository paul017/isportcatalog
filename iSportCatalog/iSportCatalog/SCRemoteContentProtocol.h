//
//  SCRemoteContentProtocol.h
//  iSportCatalog
//
//  Created by Paul017 on 9/8/15.
//
//

#import <Foundation/Foundation.h>
#import "SCRemoteContentDelegate.h"

@protocol SCRemoteContentProtocol <NSObject>

-(void) requestServerProductsWithCategoryFind:(NSString*)categoryFind andDelegate: (id<SCRemoteContentDelegate>) delegate;
-(void) requestServerProductByBarcode:(NSString*) barcode andDelegate: (id<SCRemoteContentDelegate>) delegate;

@end
