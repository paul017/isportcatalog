//
//  SCStore.m
//  iSportCatalog
//
//  Created by Paul017 on 7/3/15.
//
//

#import "SCStore.h"

@implementation SCStore

@synthesize city = _city;
@synthesize address = _address;
@synthesize phoneNumber = _phoneNumber;
@synthesize storeName = _storeName;
@synthesize storeCoordinate = _storeCoordinate;

-(SCStore *)initWithStoreName:(NSString *)storeName storeCity:(NSString *)storeCity storeAddress:(NSString *)storeAddress storePhoneNumber:(NSString *)storePhoneNumber storeLatitudeCoordinate:(CLLocationDegrees)storeLatitudeCoordinate andStoreLongitudeCoordinate:(CLLocationDegrees)storeLongitudeCoordinate{
    _storeName = storeName;
    _address = storeAddress;
    _city = storeCity;
    _phoneNumber = storePhoneNumber;
    _storeCoordinate = CLLocationCoordinate2DMake(storeLatitudeCoordinate, storeLongitudeCoordinate);
    return self;
}

@end
