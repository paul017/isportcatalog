//
//  SCuiSingleton.h
//  iSportCatalog
//
//  Created by Paul017 on 19/2/15.
//
//

#import <Foundation/Foundation.h>
#import "SCCategoryProduct.h"
#import "SCStore.h"
#import "SCProduct.h"

@interface SCuiSingleton : NSObject

@property (nonatomic) NSMutableArray *productItems;

//Menu visible status
@property (nonatomic) BOOL isSlideMenuEnabled;

//Menu Categories selected
@property (nonatomic) SCCategoryProduct *categoryProductSelected;

//Store localization selected
@property (nonatomic) SCStore *storeSelected;

//Product carusel selected
@property (nonatomic) SCProduct *productSelected;

//Image carousel show
@property (nonatomic) UIImage *productPrymaryImage;

@end
